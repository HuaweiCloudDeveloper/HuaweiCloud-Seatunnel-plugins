package org.apache.seatunnel;

import org.junit.jupiter.api.Test;

public class Generator {

    @Test
    public void generator() throws Exception {
        RSAGenerator rsaGenerator = new RSAGenerator();
    }

    @Test
    public void RSAEncrypt() throws Exception {
        RSA rsa = new RSA();
        String encrypt = rsa.encrypt("12345678abcdefg");
        System.out.println(encrypt);
    }

    @Test
    public void RSADecrypt() throws Exception {
        RSA rsa = new RSA();
        String s = rsa.decrypt("");
        System.out.println(s);
    }

    @Test
    public void test() throws Exception {
        RSA rsa1 = new RSA();
        String encrypt = rsa1.encrypt("12345678abcdefg");
        System.out.println("--------------");
        System.out.println(encrypt);
        System.out.println("--------------");
        RSA rsa2 = new RSA();
        String s = rsa2.decrypt(encrypt);
        System.out.println(s);
    }
}
