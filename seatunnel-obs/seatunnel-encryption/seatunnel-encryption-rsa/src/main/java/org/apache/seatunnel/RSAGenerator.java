package org.apache.seatunnel;

import java.io.File;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;

public class RSAGenerator {

    public RSAGenerator() throws Exception {
        // Randomly generate a pair of keys (including public key and private key)
        KeyPair keyPair = RSAUtils.generateKeyPair();
        // Get public and private keys
        PublicKey pubKey = keyPair.getPublic();
        PrivateKey priKey = keyPair.getPrivate();
        // Save the public and private keys
        RSAUtils.saveKeyForEncodedBase64(pubKey, new File("pub.txt"));
        RSAUtils.saveKeyForEncodedBase64(priKey, new File("pri.txt"));
    }
}
