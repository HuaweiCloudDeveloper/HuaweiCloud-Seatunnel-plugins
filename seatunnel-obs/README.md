# Seatunnel扩展华为云OBS

# 项目介绍

## Seatunnel项目介绍

https://github.com/apache/seatunnel

## 扩展模块

扩展模块名为seatunnel-file-obs，是一个连接器插件，该模块中主要包含了source包和sink包，source实现了从华为云obs中读取文件，而sink包实现了将文件写到华为云obs。

## 扩展经历

[seatunnel-obs 扩展经历](doc/README.md)

# 源码编译部署

## 下载源码

可以直接在[(Seatunnel).(github.com)](https://github.com/apache/seatunnel)下载源码，也可以用以下命令拉取源码：

   ```
   git clone https://github.com/apache/seatunnel.git
   ```

## OBS插件集成

1. 先将本仓库seatunnel-obs下connector-file-obs模块加入到源码/seatunnel-connectors-v2/connectors-file/下，如下图所示：

  <img src="readme_images/img_6.png"/><br/>

2. 修改/seatunnel-connectors-v2/connectors-file/pom.xml，新增module如下图所示：

  <img src="readme_images/img_7.png"/><br/>

## RSA插件集成

### 集成步骤

1. 先将本仓库seatunnel-obs下seatunnel-encryption模块加入到源码根目录/下，如下图所示：

  <img src="readme_images/img_8.png"/><br/>

2. 修改根目录/pom.xml，新增module如下图所示：

  <img src="readme_images/img_9.png"/><br/>

### Seatunnel加解密官方介绍

[Config File Encryption And Decryption | Apache SeaTunnel](https://seatunnel.apache.org/docs/connector-v2/Config-Encryption-Decryption)

### RSA加密实现

1. 定义了一个RSAUtils工具类，用于实现RSA公私密钥的生成和获取方法以及对文件的加密解密。

2. 定义了一个RSAGenerator类用于生成密钥并将密钥保存到文件。

3. 由于需要文件流输入输出，因此又定义了一个IOUtils类实现在文件中的输入输出。

4. 最后创建一个RSA类实现ConfigShade接口。

### 自定义

参考实现：[Config File Encryption And Decryption | Apache SeaTunnel](https://seatunnel.apache.org/docs/connector-v2/Config-Encryption-Decryption/)

## 编译安装

详细过程请参考：[编译文档](https://github.com/apache/seatunnel/blob/dev/docs/en/contribution/setup.md)

# Seatunnel安装与使用

Seatunnel启动的前提：要有java8以上的环境

## Seatunnel的启动

Linux上安装Seatunnel：首先进入Linux中要部署项目的文件位置${SEATUNNEL_HOME}，运行以下命令便可以将官方的seatunnel项目部署完成：

```shell
export version="2.3.2"
wget "https://archive.apache.org/dist/seatunnel/${version}/apache-seatunnel-${version}-bin.tar.gz"
tar -xzvf "apache-seatunnel-${version}-bin.tar.gz"
```

决定所需要的connector插件，对config/plugin_config进行配置。

配置完后执行以下命令：

```shell
cd ${SEATUNNEL_HOME}
./bin/install-plugin.sh 2.3.2
```

此命令会下载配置文件中指定的connector，这样便可以使用指定的connector了。

在${SEATUNNEL_HOME}/connector/seatunnel目录下，可以查看已下载的connector。

接下来需要部署connector-file-obs。

上传在经过编译安装后生成的/seatunnel-connectors-v2/connectors-file/connector-file-obs/下的connector-file-obs.jar，到${SEATUNNEL_HOME}/connectors/seatunnel/路径下

再修改${SEATUNNEL_HOME}/connectors/plugins-mapping.properties

例如，添加：

```bash
seatunnel.source.ObsFile = connector-file-obs
seatunnel.sink.ObsFile = connector-file-obs
```

然后修改config/plugin_config，确定要使用的插件：

```bash
--connectors-v2--
connector-file-obs
connector-fake
connector-console
--end--
```

>其中--connectors-v2--和--end--不可以删除

## 连接器OBS的配置文件和参数

运行的配置文件主要由4大部分组成：env,source,transform和sink。

env中用于添加一些引擎可选参数。

source用于定义seatunnel需要获取数据的位置，并将获取的数据用于下一步。可以同时定义多个source。不同的source插件有不同的参数。

transform可以将source插件获取的数据进行进一步处理，当然transform的内容可以为空不对数据进行处理。

sink用于定义seatunnel需要将数据输出到的位置。可以同时定义多个sink。不同的sink插件有不同的参数。

### env配置参数

| name                       | description                    |
|----------------------------|--------------------------------|
| job.name                   | 此参数配置任务名称                      |
| jars                       | 第三方软件包可以通过此加载                  |
| job.mode                   | 用于配置任务是处于批处理模式还是流模式            |
| checkpoint.interval        | 检查点的时间间隔                       |
| Parallelism                | 此参数配置source和sink的并行度           |
| shade.identifier           | 指定加密方法，如果不需要加密或解密配置文件，则可以忽略此选项 |

### source配置参数

| name                      | type    | required | default value       | description             |
|---------------------------|---------|----------|---------------------|-------------------------|
| path                      | string  | yes      | -                   | 源文件的文件路径                |
| file_format_type          | string  | yes      | -                   | 文件类型                    |
| access_key                | string  | yes      | -                   | accesskey               |
| security_key              | string  | yes      | -                   | securitykey             |
| endpoint                  | string  | yes      | -                   | endpoint                |
| bucket                    | string  | yes      | -                   | 桶名称                     |
| delimiter                 | string  | no       | \001                | 数据行中各列之间的分隔符。只能在txt文件使用 |
| date_format               | string  | no       | yyyy-MM-dd          | 日期格式                    |
| datetime_format           | string  | no       | yyyy-MM-dd HH:mm:ss | 日期时间格式                  |
| time_format               | string  | no       | HH:mm:ss            | 时间格式                    |
| parse_partition_from_path | boolean | no       | true                | 是否从文件路径解析分区字段           |
| hdfs_site_path            | string  | no       | -                   | hdfs-site.xml的路径        |
| kerberos_principal        | string  | no       | -                   | Kerberos principal      |
| kerberos_keytab_path      | string  | no       | -                   | Kerberos密钥选项卡文件路径       |
| skip_header_row_number    | long    | no       | 0                   | 要跳过的行数                  |
| read_partitions           | list    | no       | -                   | 用户要读取的分区                |
| read_columns              | list    | no       | -                   | 用户要读取的列列表               |
| sheet_name                | string  | no       | -                   | 待读取的工作表名称，仅对excel文件有效   |

### sink配置参数

| name                             |  type   | required | default value                              | description                                                                 |
|----------------------------------|---------|----------|--------------------------------------------|-----------------------------------------------------------------------------|
| path                             | string  | yes      | -                                          | 目标文件路径                                                                      |
| bucket                           | string  | yes      | -                                          | 桶名称                                                                         |
| access_key                       | string  | yes      | -                                          | accesskey                                                                   |
| security_key                     | string  | yes      | -                                          | securitykey                                                                 |
| endpoint                         | string  | yes      | -                                          | endpoint                                                                    |
| custom_filename                  | boolean | no       | false                                      | 是否自定义输出文件名                                                                  |
| file_name_expression             | string  | no       | "${transactionId}"                         | 仅在“custom_filename”为true时使用`file_name_expression描述将创建到path中的文件表达式`          |
| filename_time_format             | string  | no       | "yyyy.MM.dd"                               | 仅在“custom_filename”为true时使用。path的时间格式                                       |
| file_format_type                 | string  | no       | "csv"                                      | 文件格式类型                                                                      |
| field_delimiter                  | string  | no       | '\001'                                     | 数据行中各列之间的分隔符。仅“text”和“csv”文件格式需要                                            |
| row_delimiter                    | string  | no       | "\n"                                       | 文件中行之间的分隔符。仅“text”和“csv”文件格式需要                                              |
| date_format                      | string  | no       | yyyy-MM-dd                                 | 日期格式                                                                        |
| datetime_format                  | string  | no       | yyyy-MM-dd HH:mm:ss                        | 日期时间格式                                                                      |
| time_format                      | string  | no       | HH:mm:ss                                   | 时间格式                                                                        |
| have_partition                   | boolean | no       | false                                      | 写入数据时是否需要分区                                                                 |
| partition_by                     | array   | no       | -                                          | 分区键列表，仅当have_Partition为true时使用                                              |
| partition_dir_expression         | string  | no       | "${k0}=${v0}/${k1}=${v1}/.../${kn}=${vn}/" | 仅当have_partition为true时使用。如果指定了partition_by，我们将根据分区信息生成相应的分区目录，最终文件将放置在分区目录中 |
| is_partition_field_write_in_file | boolean | no       | false                                      | 仅当have_partition为true时使用。是否将分区字段写入文件                                        |
| sink_columns                     | array   | no       | -                                          | 哪些列需要写入文件                                                                   |
| is_enable_transaction            | boolean | no       | true                                       | 是否启用事务                                                                      |
| batch_size                       | int     | no       | 1000000                                    | 每个拆分文件的批处理大小                                                                |
| compress_codec                   | string  | no       | none                                       | 压缩编解码器                                                                      |
| common-options                   | object  | no       | -                                          |                                                                             |
| max_rows_in_memory               | int     | no       | -                                          | 内存中的最大行数，仅对excel文件有效                                                        |
| sheet_name                       | string  | no       | Sheet${Random number}                      | 待读取的工作表名称，仅对excel文件有效                                                       |

### 启动方式及命令

使用以下命令运行配置文件：

```shell
cd ${SEATUNNEL_HOME}
./bin/seatunnel.sh --config ./config/v2.batch.config.template -e local
```

### 具体参数

<img src="readme_images/img_3.png" width="450" height="350" /><br/>

### 测试

打开config目录下的local_to_obs_text.conf文件，如下图：

<img src="readme_images/img_4.png" width="400" height="400" /><br/>

在配置文件中修改access_key、security_key等配置项。 此配置文件从本地路径（_这里以我的测试路径为例_）/opt/seatunnel/data/中读取2.txt文件，并将文件传送到obs相应的路径中，传送后的文件类型为text。

然后执行以下命令开始运行：

```shell
cd ${SEATUNNEL_HOME}
./bin/seatunnel.sh --config ./config/local_to_obs_text.conf -e local
```

运行成功截图：

<img src="readme_images/img_5.png" width="700" height="150" /><br/>

## RSA加载与使用

### RSA加载

上传在经过编译安装后生成的/seatunnel-encryption/seatunnel-encryption-rsa/下的seatunnel-encryption-rsa.jar，到${SEATUNNEL_HOME}/lib路径下

### 使用RSA进行加密解密

1.在配置文件的 env 块中添加加/解密选项，此选项指示您要使用的加解密方法为RSA，如：

```bash
env {
  execution.parallelism = 2
  shade.identifier = "RSA"
}
```

2.使用基于不同计算引擎的 shell 来加密配置文件，如：

```shell
${SEATUNNEL_HOME}/bin/seatunnel.sh --config config/xxx.conf --encrypt
```

然后您可以在Linux终端中看到进行加密操作后的配置文件信息，将Linux终端中对应的加密信息复制到xxx.conf文件，执行以下命令开始运行后会自动解密：

```shell
cd ${SEATUNNEL_HOME}
./bin/seatunnel.sh --config ./config/local_to_obs_text.conf -e local
```