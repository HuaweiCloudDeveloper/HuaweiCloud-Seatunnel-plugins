# 1 Seatunnel介绍

SeaTunnel 是一个简单易用的数据集成框架。在企业中，由于开发时间或开发部门不通用，往往有多个异构的、运行在不同的软硬件平台上的信息系统同时运行。数据集成是把不同来源、格式、特点性质的数据在逻辑上或物理上有机地集中，从而为企业提供全面的数据共享。

# 2 OBS介绍

对象存储服务（OBS）是一个高可靠、高性能、高安全的基于对象的海量存储云服务,适合存储任意大小和类型的数据。其提供三种存储类别：标准存储、低频访问存储、归档存储，以满足不同存储性能的需求。OBS的整体架构是由数据桶和对象组成的，"桶"指的是OBS中存储对象的容器，每个桶都有自己的存储类别、访问权限、所属区域等属性，用户在互联网上通过桶的访问域名来定位桶。 而"对象"是OBS中数据存储的基本单位，一个对象实际是一个文件的数据与其相关属性信息的集合体。

# 3 代码实现

## 3.1 实现思路

在seatunnel中的源码中，我们可以自定义插件来实现数据从何而来，如何转换，最后到那里去。所以我们组主要思路为写一个connector-file-obs模块，其包含从华为云obs中读取数据的source插件和将数据传输到华为云obs的sink插件。

Seatunnel的connector-file模块基于Hadoop帮助我们实现了文件传输的source插件基类和sink插件基类。所以我们就想通过这些基类以及华为云的obsa-hdfs来实现主要功能。

在加密解密方面，由于SeaTunnel自带的base64安全性不够高，我们使用到了RSA对文件的AK和SK进行加解密。

## 3.2 Source和Sink插件实现

在config包中，创建了ObsConf和ObsConfig两个类，ObsConf类继承了HadoopConf类，其主要提供了Hadoop配置时所需的常量，如文件系统的实现类OBSFileSystem，配置时所需要的AK和SK等。ObsConfig类继承了BaseSourceConfig，表示在.conf配置文件中obsFile插件的属性如AK，SK，endpoint等。

<img src="doc_images/图片 1.png" /><br/>

图 1 ObsConfig

<img src="doc_images/图片 2.png" /><br/>

图 2 ObsConf

在source包中创建了ObsFileSource类和ObsFileSourceFactory类，ObsFileSource继承了BaseFileSource类。在ObsFileSource类中重写了getPluginName方法，用于表示插件的名字是什么。然后重写了prepare方法，其用于检查配置文件填写是否正确，并根据配置文件准备好读策略。然后在BaseFileSource类中开始读取文件。ObsFileSourceFactory类实现了TableSourceFactory接口，在此类中主要重写了optionRule方法，规定了配置项中哪些属性是必须的，哪些属性是可选的。

<img src="doc_images/图片 3.png" /><br/>

图 3 obsFileSinkFactory部分代码

<img src="doc_images/图片 4.png" /><br/>

图 4 ObsFileSource部分代码

在sink包中创建了ObsFileSink类和ObsFileSinkFactory类。ObsFileSink类继承了BaseFileSink类，在此类中重写了getPluginName方法，用于表示插件的名字是什么。然后重写了prepare方法，其用于检查配置文件填写是否正确。然后再ObsFileSink类中开始写文件。ObsFileSinkFactory类实现了TableSinkFactory接口，在此类中主要重写了optionRule方法，规定了配置项中哪些属性是必须的，哪些属性是可选的。

<img src="doc_images/图片 5.png" /><br/>

图 5 ObsFileSink

<img src="doc_images/图片 6.png" /><br/>

图 6 ObsFileSinkFactory部分代码

进入GitHub下载华为云obsa-hdfs源码，根据readme.md进入到相应目录后运行mvn clean install -Pdist -Dhadoop.plat.version=3.1.1 -Dhadoop.version=3.1.1 -Dmaven.test.skip=true 指令，便可以将源码编译部署到本地的maven，然后就可以在seatunnel中通过maven引用obsa-hdfs了。

<img src="doc_images/图片 7.png" /><br/>

图 7 hadoop-obs源码编译指南

## 3.3 加密解密实现

由于需要文件流输入输出，因此又定义了一个IOUtils类实现在文件中的输入输出。该类中一共有两个方法，分别实现了在文件中输入和输出的功能。

<img src="doc_images/图片 8.png" width="450" height="300" /><br/>

图 8 IOUtils类

定义了一个RSAGenerator类用于生成密钥并将密钥保存到文件。该类只有一个方法，就是实现RSA公钥和密钥的生成，并通过IOUtils类将生成的密钥输出到文件中。

<img src="doc_images/图片 9.png" width="450" height="300" /><br/>

图 9 RSAGenerator类

定义了一个RSAUtils工具类，用于实现RSA公私密钥的生成和获取方法以及对文件的加密解密。该类通过调用上述两个类实现了生成密钥、读取密钥、利用密钥对所需加（解）密的内容进行加（解）密等方法。

<img src="doc_images/图片 10.png" /><br/>

图 10 RSAUtils工具类

最后在ConfigShadeUtils中实现RSA加解密。通过继承SeaTunnel中加密用的ConfigShade类来实现用RSA对配置文件进行加密。通过RSAUtils类将ConfigShade中所需要实现的方法实现即可完成。

<img src="doc_images/图片 11.png" /><br/>

图 11 RSAConfigShade类

如果用户需要自定义加密方法和加密配置，SeaTunnel也支持。用户只需要创建一个java maven项目，在依赖项中添加模块，如下所示。

<img src="doc_images/图片 12.png" /><br/>

图 12 依赖模块

之后创建一个新类并实现ConfigShade接口，该接口具有以下方法。

<img src="doc_images/图片 13.png" /><br/>

图 13 ConfigShade所需实现的方法

完成后将其添加到org.apache.seatunnel.api.configuration.ConfigShade文件中。最后打包成jar包放入${SEATUNNEL\_HOME}/lib中即可正常使用。

# 4 运行效果

Seatunnel提供了用于测试配置文件的测试代码，我们只需要修改需要测试的配置文件的名称，测试代码就会对改配置文件进行运行测试。

<img src="doc_images/图片 14.png" /><br/>

图 14 测试代码

Seatunnel可以对不同类型的文件进行上传下载，也可以采用不同的transform插件对数据进行转换。

首先测试的是从本地读取大小为5G的text文件上传到OBS，不对数据进行转换。上传到OBS的桶为seatunnelbucket，路径为/lin1234/text，上传后的文件名为test\_text。

<img src="doc_images/图片 15.png" /><br/>

图 15 text大文件上传配置文件

上传后OBS中显示的结果：

<img src="doc_images/图片 16.png" /><br/>

图 16 text大文件上传结果

然后测试的是从本地读取文件类型为json的文件，并使用filter对数据过滤，只提取code与success属性，然后上传到OBS。上传到OBS的桶为seatunnelbucket，路径为/lin1234/json，上传后的文件名为test\_json。

<img src="doc_images/图片 17.png" /><br/>

图 17 json上传配置文件

上传后OBS中显示的结果：

<img src="doc_images/图片 18.png" /><br/>

图 18 json上传结果

然后测试的是从本地读取文件类型为csv的文件，并使用Replace对数据过滤,将name属性中的"#"转化为"\_"，然后上传到OBS。上传到OBS的桶为seatunnelbucket，路径为/lin1234/csv，上传后的文件名为test\_csv。

<img src="doc_images/图片 19.png" /><br/>

图 19 csv上传配置文件

上传后OBS中显示的结果：

<img src="doc_images/图片 20.png" /><br/>

图 20 csv上传结果

从OBS中读取文件并且输出到控制台。读取的桶为seatunnelbucket，读取的文件路径为/123，不对数据进行转换，最后输出到控制台。

<img src="doc_images/图片 21.png" /><br/>

图 21 obs读取配置文件

控制台的输出结果：

<img src="doc_images/图片 22.png" /><br/>

图 22 读取结果

加密测试：对配置文件中的access\_key和security\_key进行加密，运行后可以看到加密后的配置文件。

<img src="doc_images/图片 23.png" /><br/>

图 23 加密运行结果

解密测试：对配置文件中的access\_key和security\_key进行解密，运行后可以看到解密后的配置文件。

<img src="doc_images/图片 24.png" /><br/>

图 24 解密运行结果

# 5 问题与解决方法

问题1：

<img src="doc_images/图片 25.png" /><br/>

图 25 maven导入错误

首先在maven导入时发生了这个错误，在官方文档的帮助下，执行mvn install解决了此问题。参考网址：[http://seatunnel.incubator.apache.org/docs/2.3.1/faq](http://seatunnel.incubator.apache.org/docs/2.3.1/faq)。

问题2：

<img src="doc_images/图片 26.png" /><br/>

图 26 插件未找到错误

当我们尝试测试写好的插件时，报了这个错误，表示插件未找到。原因是在.pom文件中没有导入相应的插件模块，导入后就可以解决。Pom文件的路径为：seatunnel-examples/seatunnel-engine-examples/pom.xml

<img src="doc_images/图片 27.png" /><br/>

图 27 导入相应插件模块

问题3：

<img src="doc_images/图片 28.png" /><br/>

图 28 环境配置问题

之后再次运行时还报了HADOOP\_HOME and hadoop.home.dir are unset的问题。这是由于环境配置出现了问题，当Hadoop在windows下运行或调用远程Hadoop集群的时候，需要winutils辅助程序才能运行。下载winutils并新增系统变量便可以解决此问题。参考网址：[https://blog.csdn.net/whs0329/article/details/121878162](https://blog.csdn.net/whs0329/article/details/121878162)。

问题4：

<img src="doc_images/图片 29.png" /><br/>

图 29 检查点过期错误

在传大文件时遇到此错误，检查点在完成之前过期。原因为Checkpoint的timeout时间或tolerable-failure的数量设置的太小，修改yaml配置文件参数就可以解决此问题。Yaml配置文件路径为：seatunnel-engine/seatunnel-engine-common/src/main/resources/seatunnel.yaml

问题5：

<img src="doc_images/图片 30.png" /><br/>

图 30 Hazelcast未激活错误

在传大文件时遇到此错误，Hazelcast未激活。原因为堆栈的大小设置得不够大，通过修改jvm\_options中配置的堆栈大小就可以解决此问题。jvm\_options文件的路径为：seatunnel-engine/seatunnel-engine-common/src/main/resources/jvm\_options。

问题6：

在加密解密的测试过程中出现解密后验证失败的情况。经过排查发现错误出现在byte数组类型与String类型相互转换的过程中，错误为byte数组转String再转回byte数组长度不一致。解决方法是数据类型转换时指定编码格式为"ISO8859-1"。参考路径：[https://blog.csdn.net/weixin\_39161141/article/details/123703658](https://blog.csdn.net/weixin_39161141/article/details/123703658)。

问题7:

在上传本地文件后会在控制台的目录中出现tmp文件夹，经分析后发现是在传输文件过程中自动创建的临时目录。在fileSystemUtils中写了删除文件夹的实现方法并在FileSinkAggregatedCommitter中进行方法的调用后删除成功。

<img src="doc_images/图片 31.png" /><br/>

图 31 调用delete\_tmp\_dir的方法

<img src="doc_images/图片 32.png" /><br/>

图 32 delete\_tmp\_dir的方法实现

<img src="doc_images/图片 33.png" /><br/>

图 33 测试结果

# 6 学习总结

起初被分到华为云这个课题，我们既兴奋，同时也有出于未知的恐惧。大家都对"华为"这个词比较熟悉，但"华为云"是第一次接触。经过前两三周在官网上对华为云的初步学习后，了解到，它是一款功能强大的云计算平台，而要将其与一个较新的数据集成框架Seatunnel之间作一个结合，对我们来说，无疑是充满挑战的。

第一道难关在于华为云的陌生知识域和对Seatunnel的现有学习资料较少，对于开发无从下手。好在对接的华为的老师提前准备好了文件，召开了一次线上会议，给我们提供了官方文档链接、讲解了开发思路、分配了不同的开源项目，让我们能够从力所能及的事做起，开始行动起来。

接下来的一周，我们依照文档上附有的开源项目地址，把项目拉到本地编译器上，随之第二道难关出现了，怎样部署好项目所需环境并成功运行呢？我们在网络上各大博客和帖子里寻求答案，一有新的进展就在群里告诉其他成员。之后安装了虚拟机，并且在IntelliJ IDEA 中使用maven导入包，在.pom文件中导入相应的插件模块，最后成功地让Seatunnel在本地运行起来，解决了这个困扰了将近一周的问题。

项目环境部署好了，也能成功运行了，之后开始正式进入这次课题的内容：将华为云的OBS服务与Seatunnel对接。基于Seatunnel的特性，我们编写了从华为云OBS中读取数据的source插件和将数据传输到华为云OBS的sink插件，实现了数据从何而来，如何转换，最后到哪里去。

每次的线上交流会议，老师都会细心聆听我们的汇报，指出并记录下问题，指明接下来要编写的方向，我们则针对存在的问题，在网上学习新知识，找寻办法改进，在此期间收获颇丰。

这次学年设计，对我们来说是踏入了一个崭新的领域，开始接触实际生产应用中出现的商业技术，开拓了我们的眼界、增长了我们的见识。

我们熟悉了Seatunnel以及华为云上的知识，还锻炼了代码阅读与开发能力，累积了实战经验，为将来的工作打下了基础；还了解到在商业应用中，怎样编写源代码才算规范，并且培养了良好的编写代码的习惯；同时锻炼了自身的抗打击和受挫能力，在一个又一个困难接踵而至时，依旧不放弃，为解决难题找到办法。

最后，再次感谢这几个月里，各位老师给我们提供的帮助与支持！
