# Seatunnel

## 项目背景
[**SeaTunnel**](https://gitee.com/seatunnel/incubator-seatunnel)是一个分布式、高性能的数据集成平台，用于大量数据的同步和转换(离线和实时)。

**SeaTunnel work flowchart**
![img](https://gitee.com/seatunnel/incubator-seatunnel/raw/dev/docs/en/images/architecture_diagram.png)

## 扩展功能说明

### OBS、GaussDB-NoSQL
| 扩展对象                                                              | 部署与使用 | 开发经历 |
|----------------------------------------------------------------------|-----------|---------|
| [OBS](https://support.huaweicloud.com/obs/index.html)             |  [OBS部署与使用](seatunnel-obs/README.md)   |   [OBS开发经历](seatunnel-obs/doc/README.md)   |
| [GaussDB-NoSQL](https://support.huaweicloud.com/nosql/index.html) |  [GaussDB部署与使用](seatunnel-gaussdbnosql/README.md)     |  [GaussDB开发经历](seatunnel-gaussdbnosql/doc/README.md)     |

### DWS
下一个版本集成...

## 云商店部署

[云商店部署链接](https://marketplace.huaweicloud.com/contents/544c3ff0-19e3-4640-adc5-7ad0be2254bd#productid=OFFI895249311184281600)


