package org.apache.seatunnel.connectors.seatunnel.gaussdbmongodb.serde;

import com.mongodb.client.model.*;
import org.apache.seatunnel.api.table.type.SeaTunnelRow;
import org.apache.seatunnel.connectors.seatunnel.gaussdbmongodb.sink.MongodbWriterOptions;
import org.bson.BsonDocument;
import org.bson.conversions.Bson;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class RowDataDocumentSerializer implements DocumentSerializer<SeaTunnelRow> {

    private final RowDataToBsonConverters.RowDataToBsonConverter rowDataToBsonConverter;

    private final Boolean isUpsertEnable;

    private final Function<BsonDocument, BsonDocument> filterConditions;

    public RowDataDocumentSerializer(
            RowDataToBsonConverters.RowDataToBsonConverter rowDataToBsonConverter,
            MongodbWriterOptions options,
            Function<BsonDocument, BsonDocument> filterConditions) {
        this.rowDataToBsonConverter = rowDataToBsonConverter;
        this.isUpsertEnable = options.isUpsertEnable();
        this.filterConditions = filterConditions;
    }

    @Override
    public WriteModel<BsonDocument> serializeToWriteModel(SeaTunnelRow row) {
        final BsonDocument bsonDocument = rowDataToBsonConverter.convert(row);
        if (isUpsertEnable) {
            Bson filter = generateFilter(filterConditions.apply(bsonDocument));
            bsonDocument.remove("_id");
            BsonDocument update = new BsonDocument("$set", bsonDocument);
            return new UpdateOneModel<>(filter, update, new UpdateOptions().upsert(true));
        } else {
            return new InsertOneModel<>(bsonDocument);
        }
    }

    public static Bson generateFilter(BsonDocument filterConditions) {
        List<Bson> filters =
                filterConditions.entrySet().stream()
                        .map(entry -> Filters.eq(entry.getKey(), entry.getValue()))
                        .collect(Collectors.toList());

        return Filters.and(filters);
    }
}
