
package org.apache.seatunnel.connectors.seatunnel.gaussdbmongodb.source;

import com.google.auto.service.AutoService;
import org.apache.seatunnel.api.common.PrepareFailException;
import org.apache.seatunnel.api.source.*;
import org.apache.seatunnel.api.table.catalog.CatalogTableUtil;
import org.apache.seatunnel.api.table.type.SeaTunnelDataType;
import org.apache.seatunnel.api.table.type.SeaTunnelRow;
import org.apache.seatunnel.api.table.type.SeaTunnelRowType;
import org.apache.seatunnel.connectors.seatunnel.gaussdbmongodb.config.MongodbConfig;
import org.apache.seatunnel.connectors.seatunnel.gaussdbmongodb.internal.MongodbClientProvider;
import org.apache.seatunnel.connectors.seatunnel.gaussdbmongodb.internal.MongodbCollectionProvider;
import org.apache.seatunnel.connectors.seatunnel.gaussdbmongodb.serde.DocumentDeserializer;
import org.apache.seatunnel.connectors.seatunnel.gaussdbmongodb.serde.DocumentRowDataDeserializer;
import org.apache.seatunnel.connectors.seatunnel.gaussdbmongodb.source.split.MongoSplit;
import org.apache.seatunnel.connectors.seatunnel.gaussdbmongodb.source.split.MongoSplitStrategy;
import org.apache.seatunnel.connectors.seatunnel.gaussdbmongodb.source.split.SamplingSplitStrategy;
import org.apache.seatunnel.shade.com.typesafe.config.Config;
import org.bson.BsonDocument;

import java.util.ArrayList;

@AutoService(SeaTunnelSource.class)
public class MongodbSource
        implements SeaTunnelSource<SeaTunnelRow, MongoSplit, ArrayList<MongoSplit>>, SupportColumnProjection {

    private static final long serialVersionUID = 1L;

    private MongodbClientProvider clientProvider;

    private DocumentDeserializer<SeaTunnelRow> deserializer;

    private MongoSplitStrategy splitStrategy;

    private SeaTunnelRowType rowType;

    private MongodbReadOptions mongodbReadOptions;

    @Override
    public String getPluginName() {
        return "GaussDB-MongoDB";
    }

    @Override
    public void prepare(Config pluginConfig) throws PrepareFailException {
        if (pluginConfig.hasPath(MongodbConfig.URI.key()) && pluginConfig.hasPath(MongodbConfig.DATABASE.key())
                && pluginConfig.hasPath(MongodbConfig.COLLECTION.key())) {
            String connection = pluginConfig.getString(MongodbConfig.URI.key());
            String database = pluginConfig.getString(MongodbConfig.DATABASE.key());
            String collection = pluginConfig.getString(MongodbConfig.COLLECTION.key());
            clientProvider = MongodbCollectionProvider.builder().connectionString(connection).database(database)
                    .collection(collection).build();
        }
        if (pluginConfig.hasPath(CatalogTableUtil.SCHEMA.key())) {
            this.rowType = CatalogTableUtil.buildWithConfig(pluginConfig).getSeaTunnelRowType();
        } else {
            this.rowType = CatalogTableUtil.buildSimpleTextSchema();
        }

        if (pluginConfig.hasPath(MongodbConfig.FLAT_SYNC_STRING.key())) {
            deserializer = new DocumentRowDataDeserializer(rowType.getFieldNames(), rowType,
                    pluginConfig.getBoolean(MongodbConfig.FLAT_SYNC_STRING.key()));
        } else {
            deserializer = new DocumentRowDataDeserializer(rowType.getFieldNames(), rowType,
                    MongodbConfig.FLAT_SYNC_STRING.defaultValue());
        }

        SamplingSplitStrategy.Builder splitStrategyBuilder = SamplingSplitStrategy.builder();
        if (pluginConfig.hasPath(MongodbConfig.MATCH_QUERY.key())) {
            splitStrategyBuilder
                    .setMatchQuery(BsonDocument.parse(pluginConfig.getString(MongodbConfig.MATCH_QUERY.key())));
        }
        if (pluginConfig.hasPath(MongodbConfig.SPLIT_KEY.key())) {
            splitStrategyBuilder.setSplitKey(pluginConfig.getString(MongodbConfig.SPLIT_KEY.key()));
        }
        if (pluginConfig.hasPath(MongodbConfig.SPLIT_SIZE.key())) {
            splitStrategyBuilder.setSizePerSplit(pluginConfig.getLong(MongodbConfig.SPLIT_SIZE.key()));
        }
        if (pluginConfig.hasPath(MongodbConfig.PROJECTION.key())) {
            splitStrategyBuilder
                    .setProjection(BsonDocument.parse(pluginConfig.getString(MongodbConfig.PROJECTION.key())));
        }
        splitStrategy = splitStrategyBuilder.setClientProvider(clientProvider).build();

        MongodbReadOptions.MongoReadOptionsBuilder mongoReadOptionsBuilder = MongodbReadOptions.builder();
        if (pluginConfig.hasPath(MongodbConfig.MAX_TIME_MIN.key())) {
            mongoReadOptionsBuilder.setMaxTimeMS(pluginConfig.getLong(MongodbConfig.MAX_TIME_MIN.key()));
        }
        if (pluginConfig.hasPath(MongodbConfig.FETCH_SIZE.key())) {
            mongoReadOptionsBuilder.setFetchSize(pluginConfig.getInt(MongodbConfig.FETCH_SIZE.key()));
        }
        if (pluginConfig.hasPath(MongodbConfig.CURSOR_NO_TIMEOUT.key())) {
            mongoReadOptionsBuilder.setNoCursorTimeout(pluginConfig.getBoolean(MongodbConfig.CURSOR_NO_TIMEOUT.key()));
        }
        mongodbReadOptions = mongoReadOptionsBuilder.build();
    }

    @Override
    public Boundedness getBoundedness() {
        return Boundedness.BOUNDED;
    }

    @Override
    public SeaTunnelDataType<SeaTunnelRow> getProducedType() {
        return rowType;
    }

    @Override
    public SourceReader<SeaTunnelRow, MongoSplit> createReader(SourceReader.Context readerContext) throws Exception {
        return new MongodbReader(readerContext, clientProvider, deserializer, mongodbReadOptions);
    }

    @Override
    public SourceSplitEnumerator<MongoSplit, ArrayList<MongoSplit>> createEnumerator(
            SourceSplitEnumerator.Context<MongoSplit> enumeratorContext) throws Exception {
        return new MongodbSplitEnumerator(enumeratorContext, clientProvider, splitStrategy);
    }

    @Override
    public SourceSplitEnumerator<MongoSplit, ArrayList<MongoSplit>> restoreEnumerator(
            SourceSplitEnumerator.Context<MongoSplit> enumeratorContext, ArrayList<MongoSplit> checkpointState)
            throws Exception {
        return new MongodbSplitEnumerator(enumeratorContext, clientProvider, splitStrategy, checkpointState);
    }
}
