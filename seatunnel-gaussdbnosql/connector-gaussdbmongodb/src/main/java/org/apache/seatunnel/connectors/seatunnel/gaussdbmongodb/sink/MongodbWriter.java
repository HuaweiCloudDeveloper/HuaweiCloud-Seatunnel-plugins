package org.apache.seatunnel.connectors.seatunnel.gaussdbmongodb.sink;

import com.mongodb.MongoException;
import com.mongodb.client.model.BulkWriteOptions;
import com.mongodb.client.model.WriteModel;
import lombok.extern.slf4j.Slf4j;
import org.apache.seatunnel.api.table.type.SeaTunnelRow;
import org.apache.seatunnel.connectors.seatunnel.common.sink.AbstractSinkWriter;
import org.apache.seatunnel.connectors.seatunnel.gaussdbmongodb.exception.MongodbConnectorException;
import org.apache.seatunnel.connectors.seatunnel.gaussdbmongodb.internal.MongodbClientProvider;
import org.apache.seatunnel.connectors.seatunnel.gaussdbmongodb.internal.MongodbCollectionProvider;
import org.apache.seatunnel.connectors.seatunnel.gaussdbmongodb.serde.DocumentSerializer;
import org.bson.BsonDocument;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import static org.apache.seatunnel.common.exception.CommonErrorCode.WRITER_OPERATION_FAILED;

@Slf4j
public class MongodbWriter extends AbstractSinkWriter<SeaTunnelRow, Void> {

    private MongodbClientProvider collectionProvider;

    private final DocumentSerializer<SeaTunnelRow> serializer;

    private long bulkActions;

    private final List<WriteModel<BsonDocument>> bulkRequests;

    private int maxRetries;

    private long retryIntervalMs;

    private long batchIntervalMs;

    private volatile long lastSendTime = 0L;

    private final Context context;

    public MongodbWriter(
            DocumentSerializer<SeaTunnelRow> serializer,
            MongodbWriterOptions options,
            Context context) {
        initOptions(options);
        this.context = context;
        this.serializer = serializer;
        this.bulkRequests = new ArrayList<>();
    }

    private void initOptions(MongodbWriterOptions options) {
        this.maxRetries = options.getRetryMax();
        this.retryIntervalMs = options.getRetryInterval();
        this.collectionProvider =
                MongodbCollectionProvider.builder()
                        .connectionString(options.getConnectString())
                        .database(options.getDatabase())
                        .collection(options.getCollection())
                        .build();
        this.bulkActions = options.getFlushSize();
        this.batchIntervalMs = options.getBatchIntervalMs();
    }

    @Override
    public void write(SeaTunnelRow o) throws IOException {
        bulkRequests.add(serializer.serializeToWriteModel(o));
        if (isOverMaxBatchSizeLimit() || isOverMaxBatchIntervalLimit()) {
            doBulkWrite();
        }
    }

    @Override
    public Optional<Void> prepareCommit() {
        doBulkWrite();
        return Optional.empty();
    }

    @Override
    public void close() throws IOException {
        doBulkWrite();
        if (collectionProvider != null) {
            collectionProvider.close();
        }
    }

    synchronized void doBulkWrite() {
        if (bulkRequests.isEmpty()) {
            // no records to write
            return;
        }

        boolean success =
                IntStream.rangeClosed(0, maxRetries)
                        .anyMatch(
                                i -> {
                                    try {
                                        lastSendTime = System.currentTimeMillis();
                                        collectionProvider
                                                .getDefaultCollection()
                                                .bulkWrite(
                                                        bulkRequests,
                                                        new BulkWriteOptions().ordered(true));
                                        bulkRequests.clear();
                                        return true;
                                    } catch (MongoException e) {
                                        log.debug(
                                                "Bulk Write to MongoDB failed, retry times = {}",
                                                i,
                                                e);
                                        if (i >= maxRetries) {
                                            throw new MongodbConnectorException(
                                                    WRITER_OPERATION_FAILED,
                                                    "Bulk Write to MongoDB failed",
                                                    e);
                                        }
                                        try {
                                            TimeUnit.MILLISECONDS.sleep(retryIntervalMs * (i + 1));
                                        } catch (InterruptedException ex) {
                                            Thread.currentThread().interrupt();
                                            throw new MongodbConnectorException(
                                                    WRITER_OPERATION_FAILED,
                                                    "Unable to flush; interrupted while doing another attempt",
                                                    e);
                                        }
                                        return false;
                                    }
                                });

        if (!success) {
            throw new MongodbConnectorException(
                    WRITER_OPERATION_FAILED, "Bulk Write to MongoDB failed after max retries");
        }
    }

    private boolean isOverMaxBatchSizeLimit() {
        return bulkActions != -1 && bulkRequests.size() >= bulkActions;
    }

    private boolean isOverMaxBatchIntervalLimit() {
        long lastSentInterval = System.currentTimeMillis() - lastSendTime;
        return batchIntervalMs != -1 && lastSentInterval >= batchIntervalMs;
    }
}
