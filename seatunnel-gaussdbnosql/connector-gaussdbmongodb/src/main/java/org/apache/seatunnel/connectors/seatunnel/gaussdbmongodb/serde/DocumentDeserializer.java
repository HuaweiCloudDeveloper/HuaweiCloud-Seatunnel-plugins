package org.apache.seatunnel.connectors.seatunnel.gaussdbmongodb.serde;

import org.bson.BsonDocument;

import java.io.Serializable;

public interface DocumentDeserializer<T> extends Serializable {

    T deserialize(BsonDocument bsonDocument);
}
