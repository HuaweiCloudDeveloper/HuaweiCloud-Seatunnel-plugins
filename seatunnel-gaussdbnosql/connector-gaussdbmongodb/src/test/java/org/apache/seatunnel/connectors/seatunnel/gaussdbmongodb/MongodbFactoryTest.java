package org.apache.seatunnel.connectors.seatunnel.gaussdbmongodb;

import org.apache.seatunnel.connectors.seatunnel.gaussdbmongodb.sink.MongodbSinkFactory;
import org.apache.seatunnel.connectors.seatunnel.gaussdbmongodb.source.MongodbSourceFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class MongodbFactoryTest {

    @Test
    void optionRule() {
        Assertions.assertNotNull((new MongodbSourceFactory()).optionRule());
        Assertions.assertNotNull((new MongodbSinkFactory()).optionRule());
    }
}