# 开发心得

SeaTunnel 是一个简单易用的数据集成框架。在企业中，由于开发时间或开发部门不通用，往往有多个异构的、运行在不同的软硬件平台上的信息系统同时运行。数据集成是把不同来源、格式、特点性质的数据在逻辑上或物理上有机地集中，从而为企业提供全面的数据共享。

![architecture_diagram](../images/architecture_diagram.png)

根据上面的图形描述，在seatunnel中，我们主要可以主要可以做的就是三部分：定义数据来源，将数据进行转化，再将数据进行输出。

可以实现不同数据源之间的沟通，比如MySQL的数据源转化成Oracle的数据源。

我们组主要新建的是连接器的插件编写，在seatunnel-connectors-v2目录下新建一个module，命名为connector-{连接器名}。比如说这个华为云的GaussDB-NoSQL有不用的数据库接口，分别是Cassandra、MongoDB、InfluxDB、Redis，我们要做的就是进行不同接口的适配工作并且测试。

连接器的主要项目结构如下图所示：

![image-20230724152435683](../images/image-20230724152435683.png)

## 1. Cassandra

GaussDB-NoSQL中，该版本是3.11

com.datastax.oss:java-driver-core:4.14.0是DataStax Java驱动程序的一个版本，该驱动程序用于与Cassandra数据库进行交互。关于兼容性问题，这个版本的驱动程序确实与Cassandra 3.11兼容。

对于com.datastax.oss:java-driver-core:4.14.0来说，它是为Cassandra 3.0及更高版本设计的，所以它与Cassandra 3.11是兼容的。

![wps1](../images/wps1.jpg)

​	在sink与source获取到config中的配置，使用client进行链接数据，TypeConvertUtil进行数据类型转换，执行过程中有错误则调用exception中的实现类。

![image-20230724155651606](../images/image-20230724155651606.png)

### *GaussDBCassandraSource：*

SeaTunnelSource是获取SourceReader，SplitEnumerator等对象以及序列化器。目前SeaTunnelSource支持的生产的数据类型必须是SeaTunnelRow类型。
通过getBoundedness 来决定当前Source是流Source还是批Source，所以可以通过动态配置的方式（参考default方法）来指定一个Source既可以为流，也可以为批。
getRowTypeInfo来得到数据的schema，connector可以选择硬编码来实现固定的schema，或者运行用户通过config配置来自定义schema。

### *GaussDBCassandraSourceReader：*

直接和数据源进行交互的接口，通过实现该接口完成从数据源读取数据的动作。
pollNext便是Reader的核心，通过这个接口，实现读取数据源的数据然后返回给SeaTunnel的流程。每当准备将数据传递给SeaTunnel时，就可以调用参数中的Collector.collect方法，可以无限次的调用该方法完成数据的大量读取。但是现阶段支持的数据格式只能是SeaTunnelRow。

```
try {
   ResultSet resultSet = session.execute( GaussDBClient.createSimpleStatement(                          gaussDBCassandraParameters.getCql())); 
   resultSet.forEach(row -> output.collect(TypeConvertUtil.buildSeaTunnelRow(row))); 
} finally {
   this.readerContext.signalNoMoreElement(); 
}
```

>首先执行CQL语句，获取查询结果集resultSet。
然后对resultSet进行遍历，将每一行数据转换为SeaTunnelRow类型，并通过output.collect方法输出到下游。</br>
​将Cassandra的Row对象转换为SeaTunnelRow类型的工具方法 该方法根据rowTypeInfo中的元数据信息获取每个字段的值，并生成SeaTunnelRow对象。</br>
​最后，通过readerContext.signalNoMoreElement()方法告知Flink， 数据源已经没有更多数据了，可以关闭该数据源的处理操作。</br>
​该方法实现了从数据源中获取下一个数据并输出的功能，是数据源的核心方法。 它将数据源和Flink的数据处理框架连接起来，将数据源中的数据转换为Flink可处理的数据类型，并输出到下游，为后续的数据处理提供了数据来源。

### *GaussDBCassandraSink：*

用于定义数据写入目标端的方式，实现获取SinkWriter等。
所做的是也是准备，prepare，实现获取到gaussDBParameters, seaTunnelRowType,tableSchema。

### *GaussDBCassandraSinkWriter：*

用于直接和输出源进行交互，将SeaTunnel通过数据源取得的数据提供给Writer进行数据写入。
write负责将数据传入SinkWriter，可以选择直接写入，或者缓存到一定数据后再写入，目前数据类型只支持SeaTunnelRow。

```
BoundStatement boundStatement = this.preparedStatement.bind();
addIntoBatch(row, boundStatement);
if (counter.getAndIncrement() >= gaussDBParameters.getBatchSize()) {
   flush(); 
   counter.set(0); 
}
```

主要任务是将传入的数据写入到Cassandra数据库中。 首先，使用预编译语句创建BoundStatement对象。 然后，将传递在数据进批处理addIntoBatch接下来，如果计数器的值达到了配置信息中指定的批量大小， 则调用flush方法将数据批量写入到Cassandra数据库中，并将计数置为0。该方法的目的是通过批量写入的方式提高写入数据的性能，并且确保数据的一致性。

**_​两个比较关键的函数：flush()和addIntoBatch(row, boundStatement)_**

>​flush()主要任务是将缓存的数据批量写入到Cassandra数据库中。该方法首先判断Cassandra配置信息中是否开启了异步写入模式，如果开启了，则将批量写入操作添加到completionStages列表中，并在写入完成后检查是否出现了错误。如果没有开启异步写入模式，则使用session.execute方法将批量写入操作直接发送给Cassandra数据库，并在出现异常时将批量操作转换为单条插入操作逐个执行。</br>
​无论是哪种模式，最后都会将batchStatement和boundStatementList清空，以便下一次写入操作的使用。该方法的目的是将缓存的数据一次性批量写入到Cassandra数据库中，以提高数据写入的效率，并且保证数据的一致性。如果异步写入模式开启，则可以提高写入速度，但需要检查写入结果是否正确。如果出现写入错误，则可以通过单条插入的方式逐一执行，确保所有数据都被正确地写入到Cassandra数据库中。

>​addIntoBatch主要任务是将一条数据添加到批量写入操作中。该方法首先使用for循环遍历所有字段，获取字段名、字段类型和字段值，并将字段值重新转换成对应的数据类型。</br>
​然后，将转换后的值注入到预编译语句的对应位置中，并根据Cassandra配置信息中的异步写入模式将预编译语句添加到不同的列表中。 如果异步写入模式开启，则使用session.executeAsync方法将预编译语句添加到completionStages列表中，否则将预编译语句添加到boundStatementList列表中。 如果执行过程中出现异常，则抛出GuassDBConnectorException异常。</br>
​该方法的目的是将一条数据添加到批量写入操作中，以便一次性将多条数据插入到Cassandra数据库中。方法中使用预编译语句可以提高数据写入的效率，并且避免SQL注入攻击。</br>
方法中的异步写入模式可以进一步提高写入速度，但需要在flush方法中检查写入结果是否正确。

**_使用BoundStatement有以下几个优点：_**

提高性能：BoundStatement将查询语句预处理，将查询语句和参数分开存储，可以减少查询执行时的网络传输量和服务器端对查询语句进行解析的时间，从而提高查询性能。防止SQL注入攻击：</br></br>
BoundStatement可以减少SQL注入攻击的风险，因为参数值是预编译的，不会直接拼接到SQL语句中。
方便的参数化查询：BoundStatement支持参数化查询，可以方便地将查询语句中的参数与具体的值进行绑定，避免手动拼接 SQL 语句所带来的繁琐和错误风险。</br></br>
因此，使用BoundStatement可以提高查询性能，确保安全性，并且方便地进行参数化查询。

## 2. Redis

![image-20230724160423587](../images/image-20230724160423587.png)

​Jedis 4.2.2 兼容 Redis 5。Jedis 4.x 版本系列已经对 Redis 5 进行了兼容性测试，可以与 Redis 5 无缝集成，支持所有 Redis 5 的新特性和命令。</br></br>
​Jedis的4.2.2版本不支持Redis 5以下的版本，因此无法适配Redis 5及以下的版本。Jedis 4.2.2版本是基于Redis 5的新特性和协议进行开发的，包含了一些Redis 5的新功能和API接口，因此无法与Redis 5以下的版本兼容。如果需要使用jedis操作Redis 5以下的版本，可以考虑使用Jedis 3.x版本，具体版本号根据Redis的实际版本号来选择。

在华为云中，我们开通的数据库就是Redis5，所以采用的驱动为Jedis4.2.2。由于GaussDB(for Redis)的“Proxy集群”架构提供了统一的负载均衡地址，并提供高可用能力，因此推荐使用JedisPool单机模式轻松接入。

此外，GaussDB(for Redis)对JedisSentinelPool、JedisCluster两种模式也提供支持。
下面是该模块的结构：

![image-20230724160457303](../images/image-20230724160457303.png)

Config就是链接配置的参数与链接工具类
Exception是抛出异常类
Sink输出源类
Source读取源类

### GaussDBRedisSource:

​在其中的prepare方法中,该方法首先调用CheckConfigUtil.checkAllExists方法对pluginConfig对象的多个配置项进行校验，如果校验不通过，则会抛出一个RedisConnectorException异常。接着，该方法会根据pluginConfig对象中的配置项构建一个RedisParameters对象。

如果pluginConfig对象中有GaussDBRedisConfig.FORMAT配置项，则进一步判断是否有对应的schema配置项。

​如果没有，则会抛出一个RedisConnectorException异常。如果有，则根据配置项中的值创建一个JsonDeserializationSchema对象，并将其赋值给deserializationSchema变量。最后，如果pluginConfig对象中没有GaussDBRedisConfig.FORMAT配置项，则会调用CatalogTableUtil.buildSimpleTextSchema方法创建一个简单的文本schema，并将其赋值给seaTunnelRowType变量，将deserializationSchema变量赋值为null。

### GaussDBRedisSourceReader:

在其中的pollNext方法中：该方法会循环遍历Redis中符合redisParameters.getKeysPattern()的key，并获取对应的value列表。

如果deserializationSchema是null，则将value作为一个SeaTunnelRow对象的唯一字段值，放入output中；否则，根据redisParameters. getHashKeyParseMode()的值和redisDataType的类型，将value转换成相应的数据类型，并将其放入output中。

如果redisDataType是HASH类型且redisParameters.getHashKeyParseMode()是KV，则将每个key-value对作为一个数据记录处理。具体地，将value转换成一个Map<String, String>类型的对象，遍历该对象中的每个key-value对，将key作为第一个字段的值，将剩余的字段值作为一个子Map对象，将其序列化成JSON格式的字符串，并通过deserializationSchema将其反序列化成一个SeaTunnelRow对象，将其放入output中。

最后，该方法通过context.signalNoMoreElement()方法告知框架已经没有更多数据可以输出了。

### GaussDBRedisSink：

其中的prepare：确认其中必要的链接参数，HOST， PORT， KEY ，DATA_TYPE

### GaussDBRedisSinkWriter：

其中的write方法：
该方法的输入参数element是一个SeaTunnelRow类型的对象，表示从流中读取出来的一条数据记录。该方法首先将element对象序列化成一个字符串data，序列化方式由serializationSchema决定。
接着，该方法根据redisParameters.getRedisDataType()获取Redis数据类型，并根据redisParameters.getKeyField()获取Redis中的key。如果seaTunnelRowType中包含keyField字段，则将key赋值为element对象中keyField字段的值，否则将key赋值为keyField。最后，该方法将序列化后的数据data使用RedisDataType的set方法存储到Redis中，以key作为Redis中的key。

### GaussDBRedisParameters：

这个方法是有两种实现的，一种是单机版，一种是集群版，如果是集群版的，链接就要借助另一种JedisClusterWrapper自定义的包装类。

_为什么不直接使用这个JedisCluster，理由如下：_</br>
>在使用JedisCluster时，可能会遇到某些节点宕机的情况，这时候JedisCluster会通过内部的分片算法，将数据存储到其他可用节点上。因此，如果直接使用JedisCluster对象，可能会导致某些数据在宕机的节点上无法读取或写入。
为了避免这种情况，可以通过重写Jedis中的方法，使用JedisCluster对象的相应方法实现对Redis的操作。这样，在某些节点宕机的情况下，JedisCluster会自动将数据存储到其他可用节点上，从而保证数据的完整性和可靠性。

### GaussDBRedisDataType：

用于表示Redis中不同的数据类型，包括KEY、HASH、LIST、SET和ZSET。每个枚举常量都重写了set和get方法，用于在Redis中对应的数据类型中设置值和获取值。

在Java中，枚举类型在定义一组有限常量时非常有用，可以优化代码结构和逻辑。在这个枚举类中，通过定义不同的常量，可以方便地区分不同的Redis数据类型，并且在设置和获取值时，可以直接调用对应类型的方法。这样，当需要在Java中操作Redis数据时，只需要调用RedisDataType中定义的方法即可。
