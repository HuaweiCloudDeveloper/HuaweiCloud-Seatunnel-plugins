package org.apache.seatunnel.connectors.seatunnel.gaussdbredis;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.junit.jupiter.api.Test;
import redis.clients.jedis.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JedisPoolTests {

    private static final Logger logger = LoggerFactory.getLogger(JedisPoolTests.class);

    @Test
    public void testPool() {
        String pwd = "*******";

        JedisPool pool =
                new JedisPool(new GenericObjectPoolConfig(), "******", 6379, 2000, pwd);
        Jedis jedis = pool.getResource();
        try {
            logger.info("Value retrieved: {}", jedis.get("key1"));
            logger.info("Key1 set to value1: {}", jedis.set("key1", "value1"));
        } finally {
            jedis.close();
        }
        pool.destroy();
    }
    @Test
    public void testCluster() {
        String pwd = "*******";
        JedisCluster cluster =
                new JedisCluster(
                        new HostAndPort("********", 6379),
                        200,
                        2000,
                        5,
                        pwd,
                        new GenericObjectPoolConfig());
        logger.info("Value retrieved: {}", cluster.get("key1"));
        logger.info("Key1 set to value1: {}", cluster.set("key1", "value1"));
    }
    @Test
    public void pushtest() {

        String pwd = "*********";

        JedisPool pool =
                new JedisPool(new GenericObjectPoolConfig(), "*********", 6379, 2000, pwd);
        Jedis jedis = pool.getResource();

        Pipeline pipeline = jedis.pipelined();


        int batchSize = 1000;
        for (int i = 0; i < 100; i++) {
            String key = "test:key" + i;
            String value = "value" + i;
            pipeline.set(key, value);

            if (i > 0 && i % batchSize == 0) {
                pipeline.sync();
            }
        }

        pipeline.sync();

        jedis.close();
    }
    @Test
    public void deltest() {

        String pwd = "********";

        JedisPool pool =
                new JedisPool(new GenericObjectPoolConfig(), "********", 6379, 2000, pwd);
        Jedis jedis = pool.getResource();


        Pipeline pipeline = jedis.pipelined();

        int batchSize = 1000;
        for (int i = 0; i < 100; i++) {
            String key = "test:key" + i;
            pipeline.del(key);

            if (i > 0 && i % batchSize == 0) {
                pipeline.sync();
            }
        }

        pipeline.sync();


        jedis.close();
    }
}
