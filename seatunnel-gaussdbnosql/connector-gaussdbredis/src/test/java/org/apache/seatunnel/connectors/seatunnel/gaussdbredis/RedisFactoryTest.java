package org.apache.seatunnel.connectors.seatunnel.gaussdbredis;

import org.apache.seatunnel.connectors.seatunnel.gaussdbredis.sink.GaussDBRedisSinkFactory;
import org.apache.seatunnel.connectors.seatunnel.gaussdbredis.source.GaussDBRedisSourceFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class RedisFactoryTest {

    @Test
    void optionRule() {
        Assertions.assertNotNull((new GaussDBRedisSourceFactory()).optionRule());
        Assertions.assertNotNull((new GaussDBRedisSinkFactory()).optionRule());
    }
}
