
package org.apache.seatunnel.connectors.seatunnel.gaussdbredis.source;

import org.apache.seatunnel.api.serialization.DeserializationSchema;
import org.apache.seatunnel.api.source.Collector;
import org.apache.seatunnel.api.table.type.SeaTunnelDataType;
import org.apache.seatunnel.api.table.type.SeaTunnelRow;
import org.apache.seatunnel.api.table.type.SeaTunnelRowType;
import org.apache.seatunnel.common.utils.JsonUtils;
import org.apache.seatunnel.connectors.seatunnel.common.source.AbstractSingleSplitReader;
import org.apache.seatunnel.connectors.seatunnel.common.source.SingleSplitReaderContext;

import org.apache.seatunnel.connectors.seatunnel.gaussdbredis.config.GaussDBRedisConfig;
import org.apache.seatunnel.connectors.seatunnel.gaussdbredis.config.GaussDBRedisDataType;
import org.apache.seatunnel.connectors.seatunnel.gaussdbredis.config.GaussDBRedisParameters;
import redis.clients.jedis.Jedis;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import java.nio.charset.StandardCharsets;

public class GaussDBRedisSourceReader extends AbstractSingleSplitReader<SeaTunnelRow> {
    private final GaussDBRedisParameters redisParameters;
    private final SingleSplitReaderContext context;
    private final DeserializationSchema<SeaTunnelRow> deserializationSchema;
    private Jedis jedis;

    public GaussDBRedisSourceReader(
            GaussDBRedisParameters redisParameters,
            SingleSplitReaderContext context,
            DeserializationSchema<SeaTunnelRow> deserializationSchema) {
        this.redisParameters = redisParameters;
        this.context = context;
        this.deserializationSchema = deserializationSchema;
    }

    @Override
    public void open() throws Exception {
        this.jedis = redisParameters.buildJedis();
    }

    @Override
    public void close() throws IOException {
        if (Objects.nonNull(jedis)) {
            jedis.close();
        }
    }

    @Override
    public void pollNext(Collector<SeaTunnelRow> output) throws Exception {
        Set<String> keys = jedis.keys(redisParameters.getKeysPattern());
        GaussDBRedisDataType redisDataType = redisParameters.getGaussDBRedisDataType();
        for (String key : keys) {
            List<String> values = redisDataType.get(jedis, key);
            for (String value : values) {
                if (deserializationSchema == null) {
                    output.collect(new SeaTunnelRow(new Object[] {value}));
                } else {
                    if (redisParameters.getHashKeyParseMode() == GaussDBRedisConfig.HashKeyParseMode.KV
                            && redisDataType == GaussDBRedisDataType.HASH) {
                        Map<String, String> recordsMap = JsonUtils.toMap(value);
                        for (Map.Entry<String, String> entry : recordsMap.entrySet()) {
                            String k = entry.getKey();
                            String v = entry.getValue();
                            Map<String, String> valuesMap = JsonUtils.toMap(v);
                            SeaTunnelDataType<SeaTunnelRow> seaTunnelRowType =
                                    deserializationSchema.getProducedType();
                            if(seaTunnelRowType instanceof SeaTunnelRowType) {
                                valuesMap.put(((SeaTunnelRowType) seaTunnelRowType).getFieldName(0), k);
                            }
                            deserializationSchema.deserialize(
                                    JsonUtils.toJsonString(valuesMap).getBytes(), output);
                        }
                    } else {
                        deserializationSchema.deserialize(value.getBytes(StandardCharsets.UTF_8), output);
                    }
                }
            }
        }
        context.signalNoMoreElement();
    }
}
