package org.apache.seatunnel.connectors.seatunnel.gaussdbredis.source;

import com.google.auto.service.AutoService;
import org.apache.seatunnel.api.configuration.util.OptionRule;
import org.apache.seatunnel.api.source.SeaTunnelSource;
import org.apache.seatunnel.api.table.catalog.CatalogTableUtil;
import org.apache.seatunnel.api.table.factory.Factory;
import org.apache.seatunnel.api.table.factory.TableSourceFactory;
import org.apache.seatunnel.connectors.seatunnel.gaussdbredis.config.GaussDBRedisConfig;

@AutoService(Factory.class)
public class GaussDBRedisSourceFactory implements TableSourceFactory {
    @Override
    public String factoryIdentifier() {
        return "GaussDB-Redis";
    }

    @Override
    public OptionRule optionRule() {
        return OptionRule.builder()
                .required(
                        GaussDBRedisConfig.HOST, GaussDBRedisConfig.PORT, GaussDBRedisConfig.KEY, GaussDBRedisConfig.DATA_TYPE)
                .optional(
                        GaussDBRedisConfig.MODE,
                        GaussDBRedisConfig.HASH_KEY_PARSE_MODE,
                        GaussDBRedisConfig.AUTH,
                        GaussDBRedisConfig.USER,
                        GaussDBRedisConfig.KEY_PATTERN)
                .conditional(GaussDBRedisConfig.MODE, GaussDBRedisConfig.RedisMode.CLUSTER, GaussDBRedisConfig.NODES)
                .bundled(GaussDBRedisConfig.FORMAT, CatalogTableUtil.SCHEMA)
                .build();
    }

    @Override
    public Class<? extends SeaTunnelSource> getSourceClass() {
        return GaussDBRedisSource.class;
    }
}
