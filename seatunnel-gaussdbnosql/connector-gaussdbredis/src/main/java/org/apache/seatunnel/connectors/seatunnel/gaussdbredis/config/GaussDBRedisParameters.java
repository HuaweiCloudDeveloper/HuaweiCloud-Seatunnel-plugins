package org.apache.seatunnel.connectors.seatunnel.gaussdbredis.config;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.apache.seatunnel.common.exception.CommonErrorCode;
import org.apache.seatunnel.connectors.seatunnel.gaussdbredis.exception.GaussDBRedisConnectorException;
import org.apache.seatunnel.connectors.seatunnel.gaussdbredis.config.GaussDBRedisConfig;
import org.apache.seatunnel.shade.com.typesafe.config.Config;
import redis.clients.jedis.ConnectionPoolConfig;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import java.util.Locale;

@Data
public class GaussDBRedisParameters implements Serializable {
    private static final long serialVersionUID = 7590532251856415149L;
    private String host;
    private int port;
    private String auth = "";
    private String user = "";
    private String keysPattern;
    private String keyField;
    private GaussDBRedisDataType gaussDBRedisDataType;
    private GaussDBRedisConfig.RedisMode mode;
    private GaussDBRedisConfig.HashKeyParseMode hashKeyParseMode;
    private List<String> redisNodes = Collections.emptyList();

    public void buildWithConfig(Config config) {
        // set host
        this.host = config.getString(GaussDBRedisConfig.HOST.key());
        // set port
        this.port = config.getInt(GaussDBRedisConfig.PORT.key());
        // set auth
        if (config.hasPath(GaussDBRedisConfig.AUTH.key())) {
            this.auth = config.getString(GaussDBRedisConfig.AUTH.key());
        }
        // set user
        if (config.hasPath(GaussDBRedisConfig.USER.key())) {
            this.user = config.getString(GaussDBRedisConfig.USER.key());
        }
        // set mode
        if (config.hasPath(GaussDBRedisConfig.MODE.key())) {
            this.mode =
                    GaussDBRedisConfig.RedisMode.valueOf(
                            config.getString(GaussDBRedisConfig.MODE.key()).toUpperCase());
        } else {
            this.mode = GaussDBRedisConfig.MODE.defaultValue();
        }
        // set hash key mode
        if (config.hasPath(GaussDBRedisConfig.HASH_KEY_PARSE_MODE.key())) {
            this.hashKeyParseMode =
                    GaussDBRedisConfig.HashKeyParseMode.valueOf(
                            config.getString(GaussDBRedisConfig.HASH_KEY_PARSE_MODE.key()).toUpperCase());
        } else {
            this.hashKeyParseMode = GaussDBRedisConfig.HASH_KEY_PARSE_MODE.defaultValue();
        }
        // set redis nodes information
        if (config.hasPath(GaussDBRedisConfig.NODES.key())) {
            this.redisNodes = config.getStringList(GaussDBRedisConfig.NODES.key());
        }
        // set key
        if (config.hasPath(GaussDBRedisConfig.KEY.key())) {
            this.keyField = config.getString(GaussDBRedisConfig.KEY.key());
        }
        // set keysPattern
        if (config.hasPath(GaussDBRedisConfig.KEY_PATTERN.key())) {
            this.keysPattern = config.getString(GaussDBRedisConfig.KEY_PATTERN.key());
        }
        // set redis data type
        try {
            String dataType = config.getString(GaussDBRedisConfig.DATA_TYPE.key());
            this.gaussDBRedisDataType = GaussDBRedisDataType.valueOf(dataType.toUpperCase(Locale.ROOT));
        } catch (IllegalArgumentException e) {
            throw new GaussDBRedisConnectorException(
                    CommonErrorCode.UNSUPPORTED_DATA_TYPE,
                    "Redis source connector only support these data types [key, hash, list, set, zset]",
                    e);
        }
    }

    public Jedis buildJedis() {
        switch (mode) {
            case SINGLE:
                Jedis jedis = new Jedis(host, port);
                if (StringUtils.isNotBlank(auth)) {
                    jedis.auth(auth);
                }
                if (StringUtils.isNotBlank(user)) {
                    jedis.aclSetUser(user);
                }
                return jedis;
            case CLUSTER:
                HashSet<HostAndPort> nodes = new HashSet<>();
                HostAndPort node = new HostAndPort(host, port);
                nodes.add(node);
                if (!redisNodes.isEmpty()) {
                    for (String redisNode : redisNodes) {
                        String[] splits = redisNode.split(":");
                        if (splits.length != 2) {
                            throw new GaussDBRedisConnectorException(
                                    CommonErrorCode.ILLEGAL_ARGUMENT,
                                    "Invalid redis node information,"
                                            + "redis node information must like as the following: [host:port]");
                        }
                        HostAndPort hostAndPort =
                                new HostAndPort(splits[0], Integer.parseInt(splits[1]));
                        nodes.add(hostAndPort);
                    }
                }
                ConnectionPoolConfig connectionPoolConfig = new ConnectionPoolConfig();
                JedisCluster jedisCluster;
                if (StringUtils.isNotBlank(auth)) {
                    jedisCluster =
                            new JedisCluster(
                                    nodes,
                                    JedisCluster.DEFAULT_TIMEOUT,
                                    JedisCluster.DEFAULT_TIMEOUT,
                                    JedisCluster.DEFAULT_MAX_ATTEMPTS,
                                    auth,
                                    connectionPoolConfig);
                } else {
                    jedisCluster = new JedisCluster(nodes);
                }
                return new GaussDBJedisWrapper(jedisCluster);
            default:
                throw new GaussDBRedisConnectorException(
                        CommonErrorCode.UNSUPPORTED_OPERATION, "Not support this redis mode");
        }
    }
}
