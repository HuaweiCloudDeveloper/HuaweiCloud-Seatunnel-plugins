package org.apache.seatunnel.connectors.seatunnel.gaussdbredis.sink;

import com.google.auto.service.AutoService;
import org.apache.seatunnel.api.configuration.util.OptionRule;
import org.apache.seatunnel.api.table.factory.Factory;
import org.apache.seatunnel.api.table.factory.TableSinkFactory;
import org.apache.seatunnel.connectors.seatunnel.gaussdbredis.config.GaussDBRedisConfig;

@AutoService(Factory.class)
public class GaussDBRedisSinkFactory implements TableSinkFactory {
    @Override
    public String factoryIdentifier() {
        return "GaussDB-Redis";
    }

    @Override
    public OptionRule optionRule() {
        return OptionRule.builder()
                .required(
                        GaussDBRedisConfig.HOST, GaussDBRedisConfig.PORT, GaussDBRedisConfig.KEY, GaussDBRedisConfig.DATA_TYPE)
                .optional(
                        GaussDBRedisConfig.MODE,
                        GaussDBRedisConfig.AUTH,
                        GaussDBRedisConfig.USER,
                        GaussDBRedisConfig.KEY_PATTERN,
                        GaussDBRedisConfig.FORMAT)
                .conditional(GaussDBRedisConfig.MODE, GaussDBRedisConfig.RedisMode.CLUSTER, GaussDBRedisConfig.NODES)
                .build();
    }
}
