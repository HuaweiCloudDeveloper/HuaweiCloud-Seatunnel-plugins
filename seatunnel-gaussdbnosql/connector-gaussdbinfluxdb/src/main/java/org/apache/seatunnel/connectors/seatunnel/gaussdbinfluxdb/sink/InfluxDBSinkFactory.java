/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *     contributor license agreements.  See the NOTICE file distributed with
 *     this work for additional information regarding copyright ownership.
 *     The ASF licenses this file to You under the Apache License, Version 2.0
 *     (the "License"); you may not use this file except in compliance with
 *     the License.  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

package org.apache.seatunnel.connectors.seatunnel.gaussdbinfluxdb.sink;

import com.google.auto.service.AutoService;
import org.apache.seatunnel.api.configuration.util.OptionRule;
import org.apache.seatunnel.api.table.factory.Factory;
import org.apache.seatunnel.api.table.factory.TableSinkFactory;

import static org.apache.seatunnel.connectors.seatunnel.gaussdbinfluxdb.config.InfluxDBConfig.*;
import static org.apache.seatunnel.connectors.seatunnel.gaussdbinfluxdb.config.SinkConfig.*;

@AutoService(Factory.class)
public class InfluxDBSinkFactory implements TableSinkFactory {

    @Override
    public String factoryIdentifier() {
        return "GaussDB-InfluxDB";
    }

    @Override
    public OptionRule optionRule() {
        return OptionRule.builder().required(URL, DATABASES, KEY_MEASUREMENT).bundled(USERNAME, PASSWORD)
                .optional(CONNECT_TIMEOUT_MS, KEY_TAGS, KEY_TIME, BATCH_SIZE, BATCH_INTERVAL_MS, MAX_RETRIES,
                        RETRY_BACKOFF_MULTIPLIER_MS)
                .build();
    }
}
