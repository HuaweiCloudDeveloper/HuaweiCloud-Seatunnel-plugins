package org.apache.seatunnel.connectors.seatunnel.gaussdbinfluxdb;


import org.apache.seatunnel.connectors.seatunnel.gaussdbinfluxdb.sink.InfluxDBSinkFactory;
import org.apache.seatunnel.connectors.seatunnel.gaussdbinfluxdb.source.InfluxDBSourceFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class InfluxDBFactoryTest {
    @Test
    void optionRule() {
        Assertions.assertNotNull((new InfluxDBSourceFactory()).optionRule());
        Assertions.assertNotNull((new InfluxDBSinkFactory()).optionRule());
    }
}