/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.seatunnel.connectors.seatunnel.gaussdbcassandra.source;

import com.datastax.oss.driver.api.core.CqlSession;
import com.datastax.oss.driver.api.core.cql.Row;
import com.google.auto.service.AutoService;
import org.apache.seatunnel.api.common.PrepareFailException;
import org.apache.seatunnel.api.common.SeaTunnelAPIErrorCode;
import org.apache.seatunnel.api.source.Boundedness;
import org.apache.seatunnel.api.source.SeaTunnelSource;
import org.apache.seatunnel.api.source.SupportColumnProjection;
import org.apache.seatunnel.api.table.type.SeaTunnelDataType;
import org.apache.seatunnel.api.table.type.SeaTunnelRow;
import org.apache.seatunnel.api.table.type.SeaTunnelRowType;
import org.apache.seatunnel.common.config.CheckConfigUtil;
import org.apache.seatunnel.common.config.CheckResult;
import org.apache.seatunnel.common.constants.PluginType;
import org.apache.seatunnel.common.exception.CommonErrorCode;
import org.apache.seatunnel.connectors.seatunnel.common.source.AbstractSingleSplitReader;
import org.apache.seatunnel.connectors.seatunnel.common.source.AbstractSingleSplitSource;
import org.apache.seatunnel.connectors.seatunnel.common.source.SingleSplitReaderContext;
import org.apache.seatunnel.connectors.seatunnel.gaussdbcassandra.Client.GaussDBClient;
import org.apache.seatunnel.connectors.seatunnel.gaussdbcassandra.config.GaussDBCassandraParameters;
import org.apache.seatunnel.connectors.seatunnel.gaussdbcassandra.exception.GaussDBCassandraErrorCode;
import org.apache.seatunnel.connectors.seatunnel.gaussdbcassandra.exception.GaussDBConnectorException;
import org.apache.seatunnel.connectors.seatunnel.gaussdbcassandra.serialize.TypeConvertUtil;
import org.apache.seatunnel.shade.com.typesafe.config.Config;

import static org.apache.seatunnel.connectors.seatunnel.gaussdbcassandra.config.GaussDBCassandraConfig.*;

@AutoService(SeaTunnelSource.class)
public class GaussDBCassandraSource extends AbstractSingleSplitSource<SeaTunnelRow> implements SupportColumnProjection {

    private SeaTunnelRowType rowTypeInfo;

    private final GaussDBCassandraParameters cassandraParameters = new GaussDBCassandraParameters();

    @Override
    public String getPluginName() {
        return "GaussDB-Cassandra";
    }

    @Override
    public void prepare(Config config) throws PrepareFailException {
        CheckResult checkResult = CheckConfigUtil.checkAllExists(config, HOST.key(), KEYSPACE.key(), CQL.key());
        if (!checkResult.isSuccess()) {
            throw new GaussDBConnectorException(SeaTunnelAPIErrorCode.CONFIG_VALIDATION_FAILED,
                    String.format("PluginName: %s, PluginType: %s, Message: %s", getPluginName(), PluginType.SOURCE,
                            checkResult.getMsg()));
        }
        this.cassandraParameters.buildWithConfig(config);

        try (CqlSession currentSession = GaussDBClient.getCqlSessionBuilder(config.getString(HOST.key()),
                config.getString(KEYSPACE.key()), cassandraParameters.getUsername(), cassandraParameters.getPassword(),
                cassandraParameters.getDatacenter()).build()) {
            Row rs = currentSession.execute(GaussDBClient.createSimpleStatement(config.getString(CQL.key()),
                    cassandraParameters.getConsistencyLevel())).one();
            if (rs == null) {
                throw new GaussDBConnectorException(GaussDBCassandraErrorCode.NO_DATA_IN_SOURCE_TABLE,
                        "No data select from this cql: " + config.getString(CQL.key()));
            }
            int columnSize = rs.getColumnDefinitions().size();
            String[] fieldNames = new String[columnSize];
            SeaTunnelDataType<?>[] seaTunnelDataTypes = new SeaTunnelDataType[columnSize];
            for (int i = 0; i < columnSize; i++) {
                fieldNames[i] = rs.getColumnDefinitions().get(i).getName().asInternal();
                seaTunnelDataTypes[i] = TypeConvertUtil.convert(rs.getColumnDefinitions().get(i).getType());
            }
            this.rowTypeInfo = new SeaTunnelRowType(fieldNames, seaTunnelDataTypes);
        } catch (GaussDBConnectorException e) {
            throw new GaussDBConnectorException(CommonErrorCode.TABLE_SCHEMA_GET_FAILED,
                    "Get table schema from cassandra source data failed", e);
        }
    }

    @Override
    public Boundedness getBoundedness() {
        return Boundedness.BOUNDED;
    }

    /**
     * Get the properties of sql
     *
     * @return
     */
    @Override
    public SeaTunnelDataType<SeaTunnelRow> getProducedType() {
        return this.rowTypeInfo;
    }

    @Override
    public AbstractSingleSplitReader<SeaTunnelRow> createReader(SingleSplitReaderContext readerContext)
            throws Exception {
        return new GaussDBCassandraSourceReader(cassandraParameters, readerContext);
    }
}
