
package org.apache.seatunnel.connectors.seatunnel.gaussdbcassandra.exception;

import org.apache.seatunnel.common.exception.SeaTunnelErrorCode;

public enum GaussDBCassandraErrorCode implements SeaTunnelErrorCode {
    FIELD_NOT_IN_TABLE("CASSANDRA-01", "Field is not existed in target table"),
    ADD_BATCH_DATA_FAILED("CASSANDRA-02", "Add batch SeaTunnelRow data into a batch failed"),
    CLOSE_CQL_SESSION_FAILED("CASSANDRA-03", "Close cql session of cassandra failed"),
    NO_DATA_IN_SOURCE_TABLE("CASSANDRA-04", "No data in source table"),
    PARSE_IP_ADDRESS_FAILED("CASSANDRA-05", "Parse ip address from string field");

    private final String code;
    private final String description;

    GaussDBCassandraErrorCode(String code, String description) {
        this.code = code;
        this.description = description;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
