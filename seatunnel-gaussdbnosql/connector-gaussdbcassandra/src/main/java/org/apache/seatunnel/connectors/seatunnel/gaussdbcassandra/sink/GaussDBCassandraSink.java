/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.seatunnel.connectors.seatunnel.gaussdbcassandra.sink;

import com.datastax.oss.driver.api.core.CqlSession;
import com.datastax.oss.driver.api.core.cql.ColumnDefinitions;
import com.google.auto.service.AutoService;
import org.apache.seatunnel.api.common.PrepareFailException;
import org.apache.seatunnel.api.common.SeaTunnelAPIErrorCode;
import org.apache.seatunnel.api.sink.SeaTunnelSink;
import org.apache.seatunnel.api.sink.SinkWriter;
import org.apache.seatunnel.api.table.type.SeaTunnelDataType;
import org.apache.seatunnel.api.table.type.SeaTunnelRow;
import org.apache.seatunnel.api.table.type.SeaTunnelRowType;
import org.apache.seatunnel.common.config.CheckConfigUtil;
import org.apache.seatunnel.common.config.CheckResult;
import org.apache.seatunnel.common.constants.PluginType;
import org.apache.seatunnel.connectors.seatunnel.common.sink.AbstractSimpleSink;
import org.apache.seatunnel.connectors.seatunnel.common.sink.AbstractSinkWriter;
import org.apache.seatunnel.connectors.seatunnel.gaussdbcassandra.Client.GaussDBClient;
import org.apache.seatunnel.connectors.seatunnel.gaussdbcassandra.config.GaussDBCassandraParameters;
import org.apache.seatunnel.connectors.seatunnel.gaussdbcassandra.exception.GaussDBCassandraErrorCode;
import org.apache.seatunnel.connectors.seatunnel.gaussdbcassandra.exception.GaussDBConnectorException;
import org.apache.seatunnel.shade.com.typesafe.config.Config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.apache.seatunnel.connectors.seatunnel.gaussdbcassandra.config.GaussDBCassandraConfig.*;

@AutoService(SeaTunnelSink.class)
public class GaussDBCassandraSink extends AbstractSimpleSink<SeaTunnelRow, Void> {

    private final GaussDBCassandraParameters gaussDBParameters = new GaussDBCassandraParameters();

    private SeaTunnelRowType seaTunnelRowType;

    private ColumnDefinitions tableSchema;

    @Override
    public String getPluginName() {
        return "GaussDB-Cassandra";
    }

    @Override
    public void prepare(Config config) throws PrepareFailException {
        CheckResult checkResult = CheckConfigUtil.checkAllExists(config, HOST.key(), KEYSPACE.key(), TABLE.key());

        if (!checkResult.isSuccess()) {
            throw new GaussDBConnectorException(SeaTunnelAPIErrorCode.CONFIG_VALIDATION_FAILED,
                    String.format("PluginName: %s, PluginType: %s, Message: %s", getPluginName(), PluginType.SINK,
                            checkResult.getMsg()));
        }
        this.gaussDBParameters.buildWithConfig(config);
        try (CqlSession currentSession = GaussDBClient.getCqlSessionBuilder(gaussDBParameters.getHost(),
                gaussDBParameters.getKeyspace(), gaussDBParameters.getUsername(), gaussDBParameters.getPassword(),
                gaussDBParameters.getDatacenter()).build()) {
            List<String> fields = gaussDBParameters.getFields();
            this.tableSchema = GaussDBClient.getTableSchema(currentSession, config.getString(TABLE.key()));

            if (fields == null || fields.isEmpty()) {
                List<String> newFields = new ArrayList<>();
                for (int i = 0; i < tableSchema.size(); i++) {
                    newFields.add(tableSchema.get(i).getName().asInternal());
                }
                gaussDBParameters.setFields(newFields);
            } else {
                for (String field : fields) {
                    if (!tableSchema.contains(field)) {
                        throw new GaussDBConnectorException(GaussDBCassandraErrorCode.FIELD_NOT_IN_TABLE,
                                "Field " + field + " does not exist in table " + config.getString(TABLE.key()));
                    }
                }
            }
        } catch (GaussDBConnectorException e) {
            throw new GaussDBConnectorException(SeaTunnelAPIErrorCode.CONFIG_VALIDATION_FAILED,
                    String.format("PluginName: %s, PluginType: %s, Message: %s", getPluginName(), PluginType.SINK,
                            checkResult.getMsg()));
        }
    }

    @Override
    public void setTypeInfo(SeaTunnelRowType seaTunnelRowType) {
        this.seaTunnelRowType = seaTunnelRowType;
    }

    @Override
    public SeaTunnelDataType<SeaTunnelRow> getConsumedType() {
        return this.seaTunnelRowType;
    }

    @Override
    public AbstractSinkWriter<SeaTunnelRow, Void> createWriter(SinkWriter.Context context) throws IOException {
        return new GaussDBCassandraSinkWriter(gaussDBParameters, seaTunnelRowType, tableSchema);
    }
}
