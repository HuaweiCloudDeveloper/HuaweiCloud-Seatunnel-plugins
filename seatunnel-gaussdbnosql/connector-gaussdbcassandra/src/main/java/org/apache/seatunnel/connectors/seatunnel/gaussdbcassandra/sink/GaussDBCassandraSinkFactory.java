
package org.apache.seatunnel.connectors.seatunnel.gaussdbcassandra.sink;

import com.google.auto.service.AutoService;
import org.apache.seatunnel.api.configuration.util.OptionRule;
import org.apache.seatunnel.api.table.factory.Factory;
import org.apache.seatunnel.api.table.factory.TableSinkFactory;
import org.apache.seatunnel.connectors.seatunnel.gaussdbcassandra.config.GaussDBCassandraConfig;

@AutoService(Factory.class)
public class GaussDBCassandraSinkFactory implements TableSinkFactory {
    @Override
    public String factoryIdentifier() {
        return "GaussDB-Cassandra";
    }

    @Override
    public OptionRule optionRule() {
        return OptionRule.builder().required(GaussDBCassandraConfig.HOST)
                .bundled(GaussDBCassandraConfig.USERNAME, GaussDBCassandraConfig.PASSWORD)
                .optional(GaussDBCassandraConfig.KEYSPACE, GaussDBCassandraConfig.TABLE,
                        GaussDBCassandraConfig.DATACENTER, GaussDBCassandraConfig.CONSISTENCY_LEVEL,
                        GaussDBCassandraConfig.FIELDS, GaussDBCassandraConfig.BATCH_SIZE,
                        GaussDBCassandraConfig.BATCH_TYPE, GaussDBCassandraConfig.ASYNC_WRITE)
                .build();
    }
}
