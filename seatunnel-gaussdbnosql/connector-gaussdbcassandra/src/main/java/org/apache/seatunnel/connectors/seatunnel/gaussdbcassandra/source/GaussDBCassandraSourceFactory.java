
package org.apache.seatunnel.connectors.seatunnel.gaussdbcassandra.source;

import com.google.auto.service.AutoService;
import org.apache.seatunnel.api.configuration.util.OptionRule;
import org.apache.seatunnel.api.source.SeaTunnelSource;
import org.apache.seatunnel.api.table.factory.Factory;
import org.apache.seatunnel.api.table.factory.TableSourceFactory;
import org.apache.seatunnel.connectors.seatunnel.gaussdbcassandra.config.GaussDBCassandraConfig;

@AutoService(Factory.class)
public class GaussDBCassandraSourceFactory implements TableSourceFactory {
    @Override
    public String factoryIdentifier() {
        return "GaussDB-Cassandra";
    }

    @Override
    public OptionRule optionRule() {
        return OptionRule.builder().required(GaussDBCassandraConfig.HOST)
                .bundled(GaussDBCassandraConfig.USERNAME, GaussDBCassandraConfig.PASSWORD)
                .optional(GaussDBCassandraConfig.KEYSPACE, GaussDBCassandraConfig.CQL,
                        GaussDBCassandraConfig.DATACENTER, GaussDBCassandraConfig.CONSISTENCY_LEVEL)
                .build();
    }

    @Override
    public Class<? extends SeaTunnelSource> getSourceClass() {
        return GaussDBCassandraSource.class;
    }
}
