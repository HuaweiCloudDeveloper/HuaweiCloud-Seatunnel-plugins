/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.seatunnel.connectors.seatunnel.gaussdbcassandra.source;

import com.datastax.oss.driver.api.core.CqlSession;
import com.datastax.oss.driver.api.core.cql.ResultSet;
import lombok.extern.slf4j.Slf4j;
import org.apache.seatunnel.api.source.Collector;
import org.apache.seatunnel.api.table.type.SeaTunnelRow;
import org.apache.seatunnel.connectors.seatunnel.common.source.AbstractSingleSplitReader;
import org.apache.seatunnel.connectors.seatunnel.common.source.SingleSplitReaderContext;
import org.apache.seatunnel.connectors.seatunnel.gaussdbcassandra.Client.GaussDBClient;
import org.apache.seatunnel.connectors.seatunnel.gaussdbcassandra.config.GaussDBCassandraParameters;
import org.apache.seatunnel.connectors.seatunnel.gaussdbcassandra.serialize.TypeConvertUtil;

import java.io.IOException;

@Slf4j
public class GaussDBCassandraSourceReader extends AbstractSingleSplitReader<SeaTunnelRow> {
    // private final GussdbConfig cassandraConfig;
    private final GaussDBCassandraParameters gaussDBCassandraParameters;
    private final SingleSplitReaderContext readerContext;
    private CqlSession session;

    GaussDBCassandraSourceReader(GaussDBCassandraParameters cassandraParameters,
                                 SingleSplitReaderContext readerContext) {
        this.gaussDBCassandraParameters = cassandraParameters;
        this.readerContext = readerContext;
    }

    @Override
    public void open() throws Exception {
        session = GaussDBClient.getCqlSessionBuilder(gaussDBCassandraParameters.getHost(),
                gaussDBCassandraParameters.getKeyspace(), gaussDBCassandraParameters.getUsername(),
                gaussDBCassandraParameters.getPassword(), gaussDBCassandraParameters.getDatacenter()).build();
    }

    @Override
    public void close() throws IOException {
        if (session != null) {
            session.close();
        }
    }

    /**
     * This method realizes the function of obtaining the next data from the data
     * source and
     * outputting it, and is the core method of the data source. It connects the
     * data source with
     * Flink's data processing framework, converts the data in the data source into
     * data types that
     * Flink can process, and outputs them to the downstream, which provides a data
     * source for
     * subsequent data processing.
     *
     * @param output output collector.
     * @throws Exception
     */
    @Override
    public void pollNext(Collector<SeaTunnelRow> output) throws Exception {
        try {
            ResultSet resultSet = session.execute(GaussDBClient.createSimpleStatement(
                    gaussDBCassandraParameters.getCql(), gaussDBCassandraParameters.getConsistencyLevel()));
            resultSet.forEach(row -> output.collect(TypeConvertUtil.buildSeaTunnelRow(row)));
        } finally {
            this.readerContext.signalNoMoreElement();
        }
    }

    @Override
    public void notifyCheckpointComplete(long checkpointId) throws Exception {
    }
}
