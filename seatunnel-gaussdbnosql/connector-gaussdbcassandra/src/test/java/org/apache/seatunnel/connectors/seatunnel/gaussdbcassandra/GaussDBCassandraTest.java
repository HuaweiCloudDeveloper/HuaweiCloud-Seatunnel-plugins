package org.apache.seatunnel.connectors.seatunnel.gaussdbcassandra;

import com.datastax.driver.core.*;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import com.datastax.driver.core.schemabuilder.SchemaBuilder;

class GaussDBCassandraTest {
    Session session;


    /**
     * createTable
     */
    @Test
    public void createTable() {


        // Cassandra host
        String hosts = "*****";
        String hosts1 = "*****";

        int port = 8635;

        Cluster cluster =
                Cluster.builder()
                        .addContactPoint(hosts).
                        addContactPoint(hosts1).
                        withPort(port).
                        withCredentials("rwuser", "*****").
                        build();
        session = cluster.connect();
        // 使用Keyspace
//        session.execute("USE mykeyspace");

        Statement statement =
                SchemaBuilder.createTable("mykeyspace", "student")
                        .addPartitionKey("id", DataType.bigint())
                        .addColumn("address", DataType.text())
                        .addColumn("age", DataType.cint())
                        .addColumn("education", DataType.map(DataType.text(), DataType.text()))
                                .
                        //                addColumn("email", DataType.text()).
                                addColumn("gender", DataType.cint())
                        .addColumn("interest", DataType.set(DataType.text()))
                        .addColumn("phone", DataType.list(DataType.text()))
                        .addColumn("name", DataType.text())
                        .ifNotExists();
        ResultSet resultSet = session.execute(statement);
        System.out.println(resultSet.getExecutionInfo());
    }

    /**
     * updateTable
     */
    @Test
    public void updateTable() {
        // Cassandra host
        String hosts = "*****";
        String hosts1 = "*****";

        int port = 8635;

        Cluster cluster =
                Cluster.builder()
                        .addContactPoint(hosts).
                        addContactPoint(hosts1).
                        withPort(port).
                        withCredentials("rwuser", "*****").
                        build();
        session = cluster.connect();
        //        add
        SchemaBuilder.alterTable("school", "student").addColumn("email").type(DataType.text());
        //        update
        SchemaBuilder.alterTable("school", "student")
                .alterColumn("email")
                .type(DataType.set(DataType.text()));
        //        delete
        SchemaBuilder.alterTable("school", "student").dropColumn("email");
    }

    /**
     * dropTable
     */
    @Test
    public void dropTable() {
        // Cassandra host
        String hosts = "*****";
        String hosts1 = "*****";

        int port = 8635;

        Cluster cluster =
                Cluster.builder()
                        .addContactPoint(hosts).
                        addContactPoint(hosts1).
                        withPort(port).
                        withCredentials("rwuser", "*****").
                        build();
        session = cluster.connect();

        Statement statement = SchemaBuilder.dropTable("school", "student").ifExists();
        session.execute(statement);
    }

    /**
     * insertByCQL
     */
    @Test
    public void insertByCQL() {

        // Cassandra host
        String hosts = "*****";
        String hosts1 = "*****";

        int port = 8635;

        Cluster cluster =
                Cluster.builder()
                        .addContactPoint(hosts).
                        addContactPoint(hosts1).
                        withPort(port).
                        withCredentials("rwuser", "*****").
                        build();
        session = cluster.connect();
        String insertSql =
                "INSERT INTO mykeyspace.student (id,address,age,gender,name,interest, phone,education) VALUES (1013,'中山路21号',16,1,'李小仙',{'游泳', '跑步'},['010-88888888','13888888888'],{'小学' : '城市第一小学', '中学' : '城市第一中学'}) ;";
        Row one = session.execute(insertSql).one();
        System.out.println("hello word");
    }

    @Test
    public void selectBySql() {
        // Cassandra host
        String hosts = "*****";
        String hosts1 = "*****";

        int port = 8635;

        Cluster cluster =
                Cluster.builder()
                        .addContactPoint(hosts).
                        addContactPoint(hosts1).
                        withPort(port).
                        withCredentials("rwuser", "*****").
                        build();
        session = cluster.connect();
        String selectSql = "SELECT * FROM mykeyspace.student ";
        Row one = session.execute(selectSql).one();
        System.out.println(one);
    }
}
