# Seatunnel扩展华为云GaussDB-NoSQL

# 项目介绍

[apache/seatunnel: SeaTunnel is a distributed, high-performance data integration platform for the synchronization and transformation of massive data (offline & real-time). (github.com)](https://github.com/apache/seatunnel)

## 扩展功能

Seatunnel提供GaussDB-NoSQL的四种接口类型的连接器，分别是：

+ GaussDBCassandraSink
+ GaussDBCassandraSource
+ GaussDBRedisSink
+ GaussDBRedisSource
+ GaussDBInfluxDBSink
+ GaussDBInfluxDBSource
+ GaussDBMongoDBSink
+ GaussDBMongoDBSource

## 扩展经历

详细开发过程请点击[doc文档](doc/README.md)

#  源码编译部署

## 下载源码

可以直接在[(Seatunnel).(github.com)](https://github.com/apache/seatunnel)下载源码，也可以用以下命令拉取源码

   ```
   git clone https://github.com/apache/seatunnel.git
   ```

## 插件集成

1. 先将本仓库seatunnel-gaussdbnosql下的四个子模块全部加入到源码/seatunnel-connectors-v2/下，如下图所示：

    ![图片](images/image-20230822103344.png)

2. 修改/seatunnel-connectors-v2/pom.xml，新增module如下图所示：

    ![图片](images/image_20230822103911.png)

## 编译安装

详细过程请参考：[编译文档](https://github.com/apache/seatunnel/blob/dev/docs/en/contribution/setup.md)

# Seatunnel安装与使用

Seatunnel启动的前提：要有java8以上的环境

## Seatunnel的启动

Linux上安装Seatunnel：首先进入Linux中要部署项目的文件位置${SEATUNNEL_HOME}，运行以下指令便可以将官方的seatunnel项目部署完成。

```shell
export version="2.3.2"
wget "https://archive.apache.org/dist/seatunnel/${version}/apache-seatunnel-${version}-bin.tar.gz"
tar -xzvf "apache-seatunnel-${version}-bin.tar.gz"
```

决定所需要的connector插件，对config/plugin_config进行配置。

配置完后执行命令

```shell
cd ${SEATUNNEL_HOME}
./bin/install-plugin.sh 2.3.2
```

此命令会下载配置文件中指定的connector，这样便可以使用指定的connector了

在${SEATUNNEL_HOME}/connector/seatunnel目录下，可以查看已下载的connector

接下来需要部署GaussDB-NoSQL四种类型的连接器

上传在经过编译安装后生成的/seatunnel-connectors-v2/下的各种连接器的jar包，到${SEATUNNEL_HOME}/connectors/seatunnel/路径下

再修改${SEATUNNEL_HOME}/connectors/plugins-mapping.properties

例如，添加：

```bash
seatunnel.source.GaussDB-Cassandra = connector-gaussdbcassandra
seatunnel.sink.GaussDB-Cassandra = connector-gaussdbcassandra
seatunnel.source.GaussDB-Redis = connector-gaussdbredis
seatunnel.sink.GaussDB-Redis = connector-gaussdbredis
seatunnel.source.GaussDB-InfluxDB = connector-gaussdbinfluxdb
seatunnel.sink.GaussDB-InfluxDB = connector-gaussdbinfluxdb
seatunnel.source.GaussDB-MongoDB = connector-gaussdbmongodb
seatunnel.sink.GaussDB-MongoDB = connector-gaussdbmongodb
```

然后修改config/plugin_config，确定要使用的插件

```bash
--connectors-v2--
connector-gaussdbcassandra
connector-gaussdbredis
connector-gaussdbinfluxdb
connector-gaussdbmongodb
connector-fake
connector-console
--end--
```

>其中--connectors-v2--和--end--不可以删除

补充一些依赖的jar包

如果要使用mysql的话需要将mysql的驱动拷贝过来，应该是需要8系列的mysql驱动，我这里使用的是mysql-connector-java-8.0.21.jar

先往数据库里面插入一些数据

Java代码如下：

```java
    @Test
    public void pushtest() {
        String pwd = "***********";
        JedisPool pool =
                new JedisPool(new GenericObjectPoolConfig(), 
                              "*********", 6379, 2000, pwd);
        Jedis jedis = pool.getResource();
        Pipeline pipeline = jedis.pipelined();
        int batchSize = 1000;
        for (int i = 0; i < 100; i++) {
            String key = "test:key" + i;
            String value = "value" + i;
            value="\"" +value +"\"";
            pipeline.set(key, value);
            if (i > 0 && i % batchSize == 0) {
                pipeline.sync();
            }
        }
        pipeline.sync();
        jedis.close();
    }
```

新建或者修改${SEATUNNEL_HOME}/config/下的xxx.conf配置文件，举个例子：

_创建一个配置文件，目的是将GaussDB-Redis中某些键值对的值打印到linux控制台_

```bash
env {
	execution.parallelism = 1
}
source {
    GaussDB-Redis {
      host = ******
      port = 6379
      auth = "******@"
      keys = "test:key*"
      data_type = key
    }
}
sink {
	Console {
	}
}
```

执行下方命令启动

```shell
cd ${SEATUNNEL_HOME}
./bin/seatunnel.sh --config ./config/xxx.conf -e local
```

运行结果图:

![image-20230725173237338](images/image-20230725173237338.png)

## 新增的四种连接器配置详情

### GaussDB-Cassandra

#### source

| 名字              | 类型   | 是否必要 | 默认值      |
| ----------------- | ------ | -------- | ----------- |
| host              | String | Yes      | -           |
| keyspace          | String | Yes      | -           |
| cql               | String | Yes      | -           |
| username          | String | No       | -           |
| password          | String | No       | -           |
| datacenter        | String | No       | datacenter1 |
| consistency_level | String | No       | LOCAL_ONE   |

host：ip和端口，也可以是域名和端口，'cassandra1:9042,cassandra2:9042'

keyspace :数据库的keyspace

cql：查询语句

username：数据库的用户名

password：密码

datacenter：用来定义和组织节点（Node）的逻辑集合，这些节点通常位于相同的物理数据中心或云区域。主要目的是提供在多个位置部署Cassandra集群时的逻辑隔离和复制策略。区域中心，默认是datacenter1

consistency_level：用来指定读取和写入操作的一致性要求的设置。Cassandra是一个分布式数据库系统，数据会在多个节点之间进行复制和分布存储。在这种分布式环境下，数据的一致性是一个重要的概念。consistency_level决定了在读取或写入操作时，Cassandra需要从多少个节点上获取数据或写入数据才能满足一致性的要求。设置不同的一致性级别可以影响读取和写入的性能和数据的一致性。

Cassandra提供了多个一致性级别选项，常见的一致性级别包括：

1. `ONE`：读取或写入操作仅需要与一个副本节点进行交互。这是最低的一致性级别，读写速度较快，但可能出现数据不一致的情况。
2. `TWO`：读取或写入操作需要与两个副本节点进行交互。这提供了更高的一致性，但在写入操作时可能需要更多的时间。
3. `THREE`：读取或写入操作需要与三个副本节点进行交互。这进一步增加了一致性的要求，但读写操作的响应时间也会相应增加。
4. `QUORUM`：读取或写入操作需要与大多数（n/2 + 1）的副本节点进行交互。这是默认的一致性级别，可以提供良好的一致性和性能。
5. `ALL`：读取或写入操作需要与所有副本节点进行交互。这提供了最高的一致性，但会导致读写操作的响应时间变长。

举例：

```bash
source {
 GaussDB-Cassandra {
     host = "localhost:9042"
     username = "cassandra"
     password = "cassandra"
     keyspace = "test"
     cql = "select * from source_table"
     result_table_name = "source_table"
    }
}
```

#### sink

| 名字              | 类型    | 是否必要 | 默认值      |
| ----------------- | ------- | -------- | ----------- |
| host              | String  | Yes      | -           |
| keyspace          | String  | Yes      | -           |
| table             | String  | Yes      | -           |
| username          | String  | No       | -           |
| password          | String  | No       | -           |
| datacenter        | String  | No       | datacenter1 |
| consistency_level | String  | No       | LOCAL_ONE   |
| fields            | String  | No       | LOCAL_ONE   |
| batch_size        | int     | No       | 5000        |
| batch_type        | String  | No       | UNLOGGED    |
| async_write       | boolean | No       | true        |

fields：需要输出到“Cassandra”的数据字段，如果没有配置，它将自动适应根据接收器表“schema”

batch_size：写入的行数， 默认值为“5000”。

batch_type：批处理模式，默认为“UNLOGGER”

async_write：是否以异步模式写入，默认值是“true”。

举例：

```bash
sink {
 GaussDB-Cassandra {
     host = "localhost:9042"
     username = "cassandra"
     password = "cassandra"
     keyspace = "test"
    }
}
```

### GaussDB-Redis

#### source

| 名字                | 类型   | 是否必要              | 默认值 |
| ------------------- | ------ | --------------------- | ------ |
| host                | string | yes                   | -      |
| port                | int    | yes                   | -      |
| keys                | string | yes                   | -      |
| data_type           | string | yes                   | -      |
| user                | string | no                    | -      |
| auth                | string | no                    | -      |
| mode                | string | no                    | single |
| hash_key_parse_mode | string | no                    | all    |
| nodes               | list   | yes when mode=cluster | -      |
| schema              | config | yes when format=json  | -      |
| format              | string | no                    | json   |

keys：keys pattern，支持模糊键匹配，用户需要确保匹配的键是同一类型

data_type： 支持 `key` `hash` `list` `set` `zset`

mode：redis 的模式, `single`（单机） 或者 `cluster`（分布式）, 默认是 `single`

nodes：redis节点信息，在集群模式下使用，必须像下面这样的格式: ["host1:port1", "host2:port2"]

format：上游数据的格式，是json

举例子：上游的数据是

```bash
{"code":  200, "data":  "get success", "success":  true}
```

```bash
# 应该格式化如下
schema {
    fields {
        code = int
        data = string
        success = boolean
    }
}
```

hash_key_parse_mode：哈希键解析模式，支持` all` `kv `,用于告诉连接器如何解析哈希键。当设置为“all”时，连接器会将散列关键字的值视为一行，并使用模式配置对其进行解析；当设置为“kv”时，连接器会将散列关键字中的每个kv视为一行，并使用模式配置对其进行解析。

举例子：

```bash
{ 
  "001": {
    "name": "tyrantlucifer",
    "age": 26
  },
  "002": {
    "name": "Zongwen",
    "age": 26
  }
}
```

all:按照以下进行配置

```bash
schema {
  fields {
    001 {
      name = string
      age = int
    }
    002 {
      name = string
      age = int
    }
  }
}
```

| 001                             | 002                       |
| ------------------------------- | ------------------------- |
| Row(name=tyrantlucifer, age=26) | Row(name=Zongwen, age=26) |

kv：按照以下进行配置：

```bash
schema {
  fields {
    hash_key = string
    name = string
    age = int
  }
}
```

| hash_key | name          | age  |
| -------- | ------------- | ---- |
| 001      | tyrantlucifer | 26   |
| 002      | Zongwen       | 26   |

连接器将使用模式配置的第一个字段信息作为每个kv中每个k的字段名称。

举例：

```bash
GaussDB-Redis {
  host = localhost
  port = 6379
  keys = "key_test*"
  data_type = key
}
```

#### sink

| name      | type   | required              | default value |
| --------- | ------ | --------------------- | ------------- |
| host      | string | yes                   | -             |
| port      | int    | yes                   | -             |
| key       | string | yes                   | -             |
| data_type | string | yes                   | -             |
| user      | string | no                    | -             |
| auth      | string | no                    | -             |
| mode      | string | no                    | single        |
| nodes     | list   | yes when mode=cluster | -             |
| format    | string | no                    | json          |

key：要写入redis的键值。

举例：如果上游数据是：

| code | data           | success |
| ---- | -------------- | ------- |
| 200  | get success    | true    |
| 500  | internal error | false   |

如果将字段名指定为“code ”,将data_type指定为“key ”,则两个数据将被写入redis:

200--> {code:200，success:  true，  data:  get success} 

500--> {code:500，success:  false，  data:internal error} 

如果将字段名指定为“value”,将data_type指定为“关键字”,则只有一个数据将被写入redis，因为“值”不存在于上游数据的字段中：

 value--> {code:500，success:  false，  data:internal error}

举例：

```bash
GaussDB-Redis {
  host = localhost
  port = 6379
  key = age
  data_type = list
}
```

### GaussDB-InfluxDB

#### source

**选项**

| 名称                          | 类型   | 必填 | 默认值 |
| ----------------------------- | ------ | ---- | ------ |
| url                           | string | 是   | -      |
| sql                           | string | 是   | -      |
| schema                        | config | 是   | -      |
| database                      | string | 是   |        |
| username                      | string | 否   | -      |
| password                      | string | 否   | -      |
| lower_bound                   | long   | 否   | -      |
| upper_bound                   | long   | 否   | -      |
| partition_num                 | int    | 否   | -      |
| split_column                  | string | 否   | -      |
| epoch                         | string | 否   | n      |
| connect_timeout_ms            | long   | 否   | 15000  |
| query_timeout_sec             | int    | 否   | 3      |
| common-options                | config | 否   | -      |
| sql：用于搜索数据的查询 SQL。 |        |      |        |

fields：上游数据的架构信息

例如：

```bash
schema {
    fields {
        name = string
        age = int
    }
  }
```

database：`influxDB` 数据库

username：选择时的 InfluxDB 用户名

password：选择时的 InfluxDB 密码

split_column：选择时的 `split_column`

upper_bound：`split_column` 列的上界

lower_bound：`split_column` 列的下界

```bash
将 $split_column 范围分为 $partition_num 部分
如果 partition_num 为 1，则使用整个 `split_column` 范围
如果 partition_num < (upper_bound - lower_bound)，则使用 (upper_bound - lower_bound) 个分区

例如：lower_bound = 1，upper_bound = 10，partition_num = 2
sql = "select * from test where age > 0 and age < 10"

分割结果

分割 1：select * from test where ($split_column >= 1 and $split_column < 6)  and (  age > 0 and age < 10 )

分割 2：select * from test where ($split_column >= 6 and $split_column < 11) and (  age > 0 and age < 10 )

```

partition_num：选择时的 `partition_num`
epoch：返回的时间精度

- 可选值：H、m、s、MS、u、n
- 默认值：n

query_timeout_sec：选择时的 InfluxDB `query_timeout`，以秒为单位

connect_timeout_ms：连接到 InfluxDB 的超时时间，以毫秒为单位

**示例**
多并行度和多分区扫描示例：

```bash
source {
    GaussDB-InfluxDB {
        url = "http://influxdb-host:8086"
        sql = "select label, value, rt, time from test"
        database = "test"
        upper_bound = 100
        lower_bound = 1
        partition_num = 4
        split_column = "value"
        schema {
            fields {
                label = STRING
                value = INT
                rt = STRING
                time = BIGINT
            }
        }
    }
}
```

不使用分区扫描的示例：

```bash
source {
    GaussDB-InfluxDB {
        url = "http://influxdb-host:8086"
        sql = "select label, value, rt, time from test"
        database = "test"
        schema {
            fields {
                label = STRING
                value = INT
                rt = STRING
                time = BIGINT
            }
        }
    }
}
```

#### sink

**选项**

| 名称                        | 类型   | 是否必需 | 默认值                     |
| --------------------------- | ------ | -------- | -------------------------- |
| url                         | string | 是       | -                          |
| database                    | string | 是       |                            |
| measurement                 | string | 是       |                            |
| username                    | string | 否       | -                          |
| password                    | string | 否       | -                          |
| key_time                    | string | 否       | 处理时间                   |
| key_tags                    | array  | 否       | 排除 `field` 和 `key_time` |
| batch_size                  | int    | 否       | 1024                       |
| batch_interval_ms           | int    | 否       | -                          |
| max_retries                 | int    | 否       | -                          |
| retry_backoff_multiplier_ms | int    | 否       | -                          |
| connect_timeout_ms          | long   | 否       | 15000                      |
| common-options              | config | 否       | -                          |

url：连接到InfluxDB的URL，
例如：

```bash
http://influxdb-host:8086
```

database：`influxDB` 数据库的名称

measurement：`influxDB` 测量的名称

username [string]：`influxDB` 用户的用户名

password：`influxDB` 用户的密码

key_tim：在SeaTunnelRow中指定`influxDB` 测量时间戳的字段名称。如果未指定，将使用处理时间作为时间戳。

key_tags：在SeaTunnelRow中指定`influxDB` 测量标签的字段名称。如果未指定，将包括所有`influxDB` 测量字段。

batch_size：用于批量写入，当缓冲区数量达到`batch_size`或时间达到`batch_interval_ms`时，数据将刷新到InfluxDB中。

batch_interval_ms：用于批量写入，当缓冲区数量达到`batch_size`或时间达到`batch_interval_ms`时，数据将刷新到InfluxDB中。

max_retries：刷新失败的重试次数

retry_backoff_multiplier_ms：作为生成后退的下一个延迟的乘数

max_retry_backoff_ms：在尝试重新请求到`influxDB`之前等待的时间量

connect_timeout_ms：连接到InfluxDB的超时时间，以毫秒为单位

**示例**

```hocon
sink {
   GaussDB-InfluxDB {
        url = "http://influxdb-host:8086"
        database = "test"
        measurement = "sink"
        key_time = "time"
        key_tags = ["label"]
        batch_size = 1
    }
}
```

### GaussDB-MongoDB

#### source

**数据类型映射**

-----------------

下表列出了从 MongoDB BSON 类型到 Seatunnel 数据类型的字段数据类型映射。

| MongoDB BSON 类型 | Seatunnel 数据类型 |
| ----------------- | ------------------ |
| ObjectId          | STRING             |
| String            | STRING             |
| Boolean           | BOOLEAN            |
| Binary            | BINARY             |
| Int32             | INTEGER            |
| Int64             | BIGINT             |
| Double            | DOUBLE             |
| Decimal128        | DECIMAL            |
| Date              | DATE               |
| Timestamp         | TIMESTAMP          |
| Object            | ROW                |
| Array             | ARRAY              |

对于 MongoDB 中的特定类型，我们使用扩展的 JSON 格式将它们映射到 Seatunnel 的 STRING 类型。

| MongoDB BSON 类型 | Seatunnel STRING                                             |
| ----------------- | ------------------------------------------------------------ |
| Symbol            | {"_value": {"$symbol": "12"}}                                |
| RegularExpression | {"_value": {"$regularExpression": {"pattern": "^9$", "options": "i"}}} |
| JavaScript        | {"_value": {"$code": "function() { return 10; }"}}           |
| DbPointer         | {"_value": {"$dbPointer": {"$ref": "db.coll", "$id": {"$oid": "63932a00da01604af329e33c"}}}} |

**提示**

> 1.在使用 Seatunnel 中的 DECIMAL 类型时，请注意最大范围不能超过 34 位数字，这意味着您应该使用 decimal(34, 18)。

**源选项**

--------------

| 名称                 | 类型    | 是否必需 | 默认值           | 描述                                                         |
| -------------------- | ------- | -------- | ---------------- | ------------------------------------------------------------ |
| uri                  | String  | 是       | -                | MongoDB 连接 URI。                                           |
| database             | String  | 是       | -                | 要读取或写入的 MongoDB 数据库的名称。                        |
| collection           | String  | 是       | -                | 要读取或写入的 MongoDB 集合的名称。                          |
| schema               | String  | 是       | -                | MongoDB 的 BSON 和 Seatunnel 数据结构映射                    |
| match.query          | String  | 否       | -                | 在 MongoDB 中，过滤器用于查询操作的文档过滤。                |
| match.projection     | String  | 否       | -                | 在 MongoDB 中，投影用于控制查询结果中包含的字段。            |
| partition.split-key  | String  | 否       | _id              | MongoDB 分片的键。                                           |
| partition.split-size | Long    | 否       | 64 * 1024 * 1024 | MongoDB 片段的大小。                                         |
| cursor.no-timeout    | Boolean | 否       | true             | MongoDB 服务器通常在空闲游标经过一段时间（10 分钟）后超时，以防止过多的内存使用。将此选项设置为 true 可防止此情况发生。但是，如果应用程序处理当前批次文档的时间超过 30 分钟，会将会话标记为过期并关闭。 |
| fetch.size           | Int     | 否       | 2048             | 为每个批次从服务器获取的文档数量。设置适当的批量大小可以提高查询性能，并避免一次性获取大量数据导致的内存压力。 |
| max.time-min         | Long    | 否       | 600              | 此参数是 MongoDB 查询选项，用于限制查询操作的最大执行时间。maxTimeMin 的值以分钟为单位。如果查询的执行时间超过指定的时间限制，MongoDB 将终止操作并返回错误。 |
| flat.sync-string     | Boolean | 否       | true             | 通过使用 `flat.sync-string`，只能设置一个字段属性值，并且字段类型必须是字符串。此操作将在单个 MongoDB 数据条目上执行字符串映射。 |

如何创建 MongoDB 数据同步任务

以下示例演示了如何创建一个从 MongoDB 读取数据并在本地客户端上打印的数据同步作业：

```bash
# 设置要执行的任务的基本配置
env {
  execution.parallelism = 1
  job.mode = "BATCH"
}

# 创建一个连接到 MongoDB 的源
source {
  GaussDB-MongoDB {
    uri = "mongodb://user:password@127.0.0.1:27017"
    database = "test_db"
    collection = "source_table"
    schema = {
      fields {
        c_map = "map<string, string>"
        c_array = "array<int>"
        c_string = string
        c_boolean = boolean
        c_int = int
        c_bigint = bigint
        c_double = double
        c_bytes = bytes
        c_date = date
        c_decimal = "decimal(38, 18)"
        c_timestamp = timestamp
        c_row = {
          c_map = "map<string, string>"
          c_array = "array<int>"
          c_string = string
          c_boolean = boolean
          c_int = int
          c_bigint = bigint
          c_double = double
          c_bytes = bytes
          c_date = date
          c_decimal = "decimal(38, 18)"
          c_timestamp = timestamp
        }
      }
    }
  }
}

# 将读取的 MongoDB 数据打印到控制台
sink {
  Console {
    parallelism = 1
  }
}
```

**参数解释**

**MongoDB 数据库连接 URI 示例**

未经身份验证的单节点连接：

```bash
mongodb://192.168.0.100:27017/mydb
```

副本集连接：

```bash
mongodb://192.168.0.100:27017/mydb?replicaSet=xxx
```

经过身份验证的副本集连接：

```bash
mongodb://admin:password@192.168.0.100:27017/mydb?replicaSet=xxx&authSource=admin
```

多节点副本集连接：

```bash
mongodb://192.168.0.1:27017,192.168.0.2:27017,192.168.0.3:27017/mydb?replicaSet=xxx
```

分片集群连接：

```bash
mongodb://192.168.0.100:27017/mydb
```

多个 mongos 连接：

```bash
mongodb://192.168.0.1:27017,192.168.0.2:27017,192.168.0.3:27017/mydb
```

注意：URI 中的用户名和密码在拼接成连接字符串之前必须进行 URL 编码。

**MatchQuery 扫描**

在数据同步场景中，需要尽早使用 `matchQuery` 方法以减少需要由后续操作处理的文档数量，从而提高性能。
以下是使用 `match.query` 的一个简单示例：

```bash
source {
  GaussDB-MongoDB {
    uri = "mongodb://user:password@127.0.0.1:27017"
    database = "test_db"
    collection = "orders"
    match.query = "{status: \"A\"}"
    schema = {
      fields {
        id = bigint
        status = string
      }
    }
  }
}
```

以下是不同数据类型的 `MatchQuery` 查询语句示例：

```bash
查询 Boolean 类型
"{c_boolean:true}"
查询字符串类型
"{c_string:\"OCzCj\"}"
查询整数
"{c_int:2}"
查询时间类型
"{c_date:ISODate(\"2023-06-26T16:00:00.000Z\")}"
查询浮点类型
{c_double:{$gte:1.71763202185342e+308}}
```

**Projection 扫描**

在 MongoDB 中，投影用于控制查询结果中包含哪些字段。可以通过指定哪些字段需要返回和哪些字段不需要来实现此目的。
在 `find()` 方法中，可以将投影对象作为第二个参数传递。投影对象的键指示要包含或排除的字段，值为 1 表示包含，0 表示排除。
以下是一个简单示例，假设我们有一个名为 `users` 的集合：

```bash
# 仅返回 name 和 email 字段
db.users.find({}, { name: 1, email: 0 });
```

在数据同步场景中，应尽早使用投影以减少需要由后续操作处理的文档数量，从而提高性能。
以下是使用投影的一个简单示例：

```bash
source {
  GaussDB-MongoDB {
    uri = "mongodb://user:password@127.0.0.1:27017"
    database = "test_db"
    collection = "users"
    match.projection = "{ name: 1, email: 0 }"
    schema = {
      fields {
        name = string
      }
    }
  }
}

```

**分区扫描**

为了加速并行源任务实例中的数据读取，Seatunnel 提供了用于 MongoDB 集合的分区扫描功能。提供以下分区策略。
用户可以通过设置 `partition.split-key` 来控制数据分片的分片键，通过设置 `partition.split-size` 来控制分片大小。

```bash
source {
  GaussDB-MongoDB {
    uri = "mongodb://user:password@127.0.0.1:27017"
    database = "test_db"
    collection = "users"
    partition.split-key = "id"
    partition.split-size = 1024
    schema = {
      fields {
        id = bigint
        status = string
      }
    }
  }
}

```

**Flat Sync String**

通过使用 `flat.sync-string`，只能设置一个字段属性值，并且字段类型必须是字符串。
此操作将在单个 MongoDB 数据条目上执行字符串映射。

```bash
source {
  GaussDB-MongoDB {
    uri = "mongodb://user:password@127.0.0.1:27017"
    database = "test_db"
    collection = "users"
    flat.sync-string = true
    schema = {
      fields {
        data = string
      }
    }
  }
}
```

使用修改参数后的数据样本，例如以下示例：

```json
{
  "_id":{
    "$oid":"643d41f5fdc6a52e90e59cbf"
  },
  "c_map":{
    "OQBqH":"jllt",
    "rkvlO":"pbfdf",
    "pCMEX":"hczrdtve",
    "DAgdj":"t",
    "dsJag":"voo"
  },
  "c_array":[
    {
      "$numberInt":"-865590937"
    },
    {
      "$numberInt":"833905600"
    },
    {
      "$numberInt":"-1104586446"
    },
    {
      "$numberInt":"2076336780"
    },
    {
      "$numberInt":"-1028688944"
    }
  ],
  "c_string":"bddkzxr",
  "c_boolean":false,
  "c_tinyint":{
    "$numberInt":"39"
  },
  "c_smallint":{
    "$numberInt":"23672"
  },
  "c_int":{
    "$numberInt":"-495763561"
  },
  "c_bigint":{
    "$numberLong":"3768307617923954543"
  },
  "c_float":{
    "$numberDouble":"5.284220288280258E37"
  },
  "c_double":{
    "$numberDouble":"1.1706091642478246E308"
  },
  "c_bytes":{
    "$binary":{
      "base64":"ZWJ4",
      "subType":"00"
    }
  },
  "c_date":{
    "$date":{
      "$numberLong":"1686614400000"
    }
  },
  "c_decimal":{
    "$numberDecimal":"683265300"
  },
  "c_timestamp":{
    "$date":{
      "$numberLong":"1684283772000"
    }
  },
  "c_row":{
    "c_map":{
      "OQBqH":"cbrzhsktmm",
      "rkvlO":"qtaov",
      "pCMEX":"tuq",
      "DAgdj":"jzop",
      "dsJag":"vwqyxtt"
    },
    "c_array":[
      {
        "$numberInt":"1733526799"
      },
      {
        "$numberInt":"-971483501"
      },
      {
        "$numberInt":"-1716160960"
      },
      {
        "$numberInt":"-919976360"
      },
      {
        "$numberInt":"727499700"
      }
    ],
    "c_string":"oboislr",
    "c_boolean":true,
    "c_tinyint":{
      "$numberInt":"-66"
    },
    "c_smallint":{
      "$numberInt":"1308"
    },
    "c_int":{
      "$numberInt":"-1573886733"
    },
    "c_bigint":{
      "$numberLong":"4877994302999518682"
    },
    "c_float":{
      "$numberDouble":"1.5353209063652051E38"
    },
    "c_double":{
      "$numberDouble":"1.1952441956458565E308"
    },
    "c_bytes":{
      "$binary":{
        "base64":"cWx5Ymp0Yw==",
        "subType":"00"
      }
    },
    "c_date":{
      "$date":{
        "$numberLong":"1686614400000"
      }
    },
    "c_decimal":{
      "$numberDecimal":"656406177"
    },
    "c_timestamp":{
      "$date":{
        "$numberLong":"1684283772000"
      }
    }
  },
  "id":{
    "$numberInt":"2"
  }
}
```

#### sink

**数据类型映射**

-----------------

以下表格列出了从MongoDB BSON类型到Seatunnel数据类型的字段数据类型映射。

| Seatunnel数据类型 | MongoDB BSON类型 |
| ----------------- | ---------------- |
| STRING            | ObjectId         |
| STRING            | String           |
| BOOLEAN           | Boolean          |
| BINARY            | Binary           |
| INTEGER           | Int32            |
| TINYINT           | Int32            |
| SMALLINT          | Int32            |
| BIGINT            | Int64            |
| DOUBLE            | Double           |
| FLOAT             | Double           |
| DECIMAL           | Decimal128       |
| Date              | Date             |
| Timestamp         | Timestamp[Date]  |
| ROW               | Object           |
| ARRAY             | Array            |

**目标（Sink）选项**

------------

| 名称                  | 类型     | 必需 | 默认值 | 描述                                                         |
| --------------------- | -------- | ---- | ------ | ------------------------------------------------------------ |
| uri                   | String   | 是   | -      | MongoDB连接URI。                                             |
| database              | String   | 是   | -      | 要读取或写入的MongoDB数据库的名称。                          |
| collection            | String   | 是   | -      | 要读取或写入的MongoDB集合的名称。                            |
| schema                | String   | 是   | -      | MongoDB的BSON和Seatunnel数据结构映射                         |
| buffer-flush.max-rows | String   | 否   | 1000   | 指定每个批次请求的最大缓冲行数。                             |
| buffer-flush.interval | String   | 否   | 30000  | 如果写入数据库的记录失败，则指定重试时间间隔，单位为秒。     |
| retry.max             | String   | 否   | 3      | 如果写入数据库的记录失败，则指定最大重试次数。               |
| retry.interval        | Duration | 否   | 1000   | 如果写入数据库的记录失败，则指定重试时间间隔，单位为毫秒。   |
| upsert-enable         | Boolean  | 否   | false  | 是否通过upsert模式写入文档。                                 |
| upsert-key            | List     | 否   | -      | upsert的主键。仅在upsert模式下有效。键应以 `["id","name",...]` 格式表示属性。 |

 **MongoDB Sink Connector 数据刷新逻辑**
MongoDB Sink Connector 的数据刷新逻辑由三个参数共同控制：`buffer-flush.max-rows`、`buffer-flush.interval` 和 `checkpoint.interval`。

> 当满足以下任一条件时，将触发数据刷新。<br/>

**如何创建 MongoDB 数据同步任务**
以下示例演示了如何创建一个数据同步任务，将随机生成的数据写入到 MongoDB 数据库中：

```bash
# 设置任务的基本配置
env {
  execution.parallelism = 1
  job.mode = "BATCH"
  checkpoint.interval  = 1000
}

source {
  FakeSource {
      row.num = 2
      bigint.min = 0
      bigint.max = 10000000
      split.num = 1
      split.read-interval = 300
      schema {
        fields {
          c_bigint = bigint
        }
      }
    }
}

sink {
  GaussDB-MongoDB{
    uri = mongodb://user:password@127.0.0.1:27017
    database = "test"
    collection = "test"
    schema = {
      fields {
        _id = string
        c_bigint = bigint
      }
    }
  }
}
```

**参数解释**
**MongoDB 数据库连接 URI 示例**

未经身份验证的单节点连接：

```bash
mongodb://127.0.0.0:27017/mydb
```

副本集连接：

```bash
mongodb://127.0.0.0:27017/mydb?replicaSet=xxx
```

经过身份验证的副本集连接：

```bash
mongodb://admin:password@127.0.0.0:27017/mydb?replicaSet=xxx&authSource=admin
```

多节点副本集连接：

```bash
mongodb://127.0.0..1:27017,127.0.0..2:27017,127.0.0.3:27017/mydb?replicaSet=xxx
```

分片群集连接：

```bash
mongodb://127.0.0.0:27017/mydb
```

多个 mongos 连接：

```bash
mongodb://192.168.0.1:27017,192.168.0.2:27017,192.168.0.3:27017/mydb
```

注意：URI 中的用户名和密码在连接字符串中连接之前必须进行 URL 编码。

**缓冲区刷新**

```bash
sink {
  GaussDB-MongoDB {
    uri = "mongodb://user:password@127.0.0.1:27017"
    database = "test_db"
    collection = "users"
    buffer-flush.max-rows = 2000
    buffer-flush.interval = 1000
    schema = {
      fields {
        _id = string
        id = bigint
        status = string
      }
    }
  }
}
```

**为什么不建议使用事务进行操作？**

尽管 MongoDB 从 4.2 版本开始完全支持多文档事务，但并不意味着每个人都应该盲目使用它们。
事务等同于锁定、节点协调、额外开销和性能影响。
相反，使用事务的原则应该是：如果有可能，避免使用它们。
合理设计系统可以极大减少使用事务的必要性。

**幂等写操作**

通过指定明确的主键并使用 upsert 方法，可以实现幂等写入语义。

如果在配置中定义了 upsert-key，MongoDB sink 将使用 upsert 语义，而不是常规的 INSERT 语句。
我们将 upsert-key 中声明的主键组合为 MongoDB 保留的主键，并使用 upsert 模式进行写入，以确保幂等写入。
在发生故障时，Seatunnel 作业将从上次成功检查点恢复并重新处理，可能会导致在恢复期间重复处理消息。
强烈建议使用 upsert 模式，因为它有助于避免违反数据库主键约束，以及在需要重新处理记录时生成重复数据。

```bash
sink {
  GaussDB-MongoDB {
    uri = "mongodb://user:password@127.0.0.1:27017"
    database = "test_db"
    collection = "users"
    upsert-enable = true
    upsert-key = ["name","status"]
    schema = {
      fields {
        _id = string
        name = string
        status = string
      }
    }
  }
}
```

### 其他连接器

更多连接器详情可参考：[Seatunel官网文档](https://seatunnel.incubator.apache.org/zh-CN/docs/2.3.1/connector-v2/source/AmazonDynamoDB/)